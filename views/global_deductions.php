<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Global Deductions</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Global Deductions</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">


    <div class="row mb-2">
      <div class="col-12">
          <div class="float-sm-right">
            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal_add_deduction"><i class="fa fa-plus"></i> Add</button>
            <button type="button" class="btn btn-danger btn-sm" onclick="delete_deduction();"><i class="fa fa-trash"></i> Delete</button>
          </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">

            <table id="tbl_deductions" class="table table-sm table-bordered table-striped">
              <thead>
              <tr class="bg-dark">
                <th width="10px;"><input type="checkbox" id="checkAll" onchange="checkAll()"></th>
                <th width="10px">#</th>
                <th width="50px"></th>
                <th>Deduction</th>
                <th width="200px">Value</th>
              </tr>
              </thead>
              <tbody>
              </tbody>                
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </div><!--/. container-fluid -->
</section>
<!-- /.content -->

<?php require "template/modals/addDeduction_MD.php";?>
<?php require "template/modals/editDeduction_MD.php";?>

<script type="text/javascript">
  $(document).ready( function(){
    deduction();
  });

  function checkAll(){
    var cb = $("#checkAll").is(":checked");
    if(cb){
      $("input[name='cb_deduct']").prop("checked", true);
    }else{
      $("input[name='cb_deduct']").prop("checked", false);
    }
  }

  function delete_deduction(){

    var url = "../ajax/delete_deduction.php";
    var x  = confirm("Are you sure to delete?");
    var checkedVals = [];

    $('input[name=cb_deduct]:checked').map(function() {
      checkedVals.push($(this).val());
    });

    if(x){
      if(checkedVals != ""){
        $.post(url,{id: checkedVals},function(data){
          if(data == 1){
            iziAlert("fa fa-check","Success! ,","Selected deduction deleted.","bottomLeft","success");
            deduction();
          }else{
           iziAlert("fa fa-ban","Error! ,","Something was wrong.","bottomLeft","error");
          }
        });
      }else{
        iziAlert("fa fa-info","Warning! ,","No deduction selected.","bottomLeft","warning");
      }
    }
   
  }

  function edit_deduct(id){
    var url = "../ajax/edit_deduction.php";

    $.post(url,{id: id},function(data){
      if(data){
        $("#editDeduction_form").html(data);
        $("#modal_edit_deduct").modal();
      }
    });
  }

  function deduction(){
    $("#tbl_deductions").DataTable().destroy();
    $("#tbl_deductions").DataTable({
      "processing":true,
      "order": [[ 1, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/deduct.php",
        "data":{
          test:"1"
        }
      },
      "order":[1,"asc"],
      "columnDefs":[{targets:[0,2],orderable: false}],
      "columns":[
        {
          "mRender": function(data,type,row){
            return "<input type='checkbox' name='cb_deduct' value='"+row.fees_id+"' "+row.disable+">";
          }
        },
        {
          "data":"count"
        },
        {
          "mRender": function(data,type,row){
            
            return "<div class='dropdown'>"+
                      "<a href='#' class='btn btn-sm btn-outline-dark' onclick='edit_deduct("+row.fees_id+")'><i class='fa fa-edit mr-1'></i> Edit</a>"+
                    "</div>";
          }
        },
        {
          "data":"fees_name"
        },
        {
          "data":"fees_value"
        }
      ]
    });
  }

</script>