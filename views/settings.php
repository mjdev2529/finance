
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Global Settings</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Global Settings</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <hr>
      </div><!-- /.container-fluid -->
    </div>

    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-secondary">
              <!-- /.card-header -->
              <!-- form start -->
              <form id="update_form" role="form" method="post" action="">
                <div class="card-body row">
                  <div class="col-6">
                    <legend>Miscellaneous</legend>
                    <hr>
                    <div class="form-group">
                      <span>Company Name</span>
                      <input type="text" class="form-control form-control-sm" name="c_name" id="c_name" placeholder="Company Name" required="">
                    </div>
                    <div class="form-group">
                      <span>Rate</span>
                      <input type="text" class="form-control form-control-sm" name="rate" id="rate" placeholder="Rate" required="">
                    </div>
                  </div>
                  <div class="form-group col-6">
	                  <legend>Document Numbers</legend>
                    <hr>
	                  <div class="mb-3">
                      <span>Check Number</span>
  	                  <input type="number" class="form-control form-control-sm" name="check_num" id="check_num" placeholder="Check Number" required="">
	                  </div>
                    <div class="mb-3">
                      <span>Official Receipt Number</span>
                      <input type="number" class="form-control form-control-sm" name="OR_num" id="OR_num" placeholder="OR Number" required="">
                    </div>
                    </div>                
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" id="btn-update" class="btn btn-secondary float-right btn-sm"><i class="fa fa-check"></i> Save Changes</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
      </div>
    </section>
    <!-- /.content -->

<script type="text/javascript">
  $(document).ready( function(){
    getUserDetails();
  });
  function getUserDetails(){
    var id = "<?php echo $_SESSION['user_id']?>";
    var url = "../ajax/getSettingsData.php";

    $.post(url,{id: id}, function(data){
      var o = JSON.parse(data);
      $("#c_name").val(o.global_name);
      $("#rate").val(o.global_value);
      $("#check_num").val(o.check_num);
      $("#OR_num").val(o.or_num);
    });

  }

  $("#update_form").submit( function(e){
    e.preventDefault();
    var url = "../ajax/update_setting.php";
    var data = $(this).serialize();


    $("#btn-update").prop("disabled",true);
    $("#btn-update").html("<i class='fa fa-spin fa-sync'></i>");
      $.post(url,data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success!, ","Global Settings successfully updated.","bottomLeft","success");
          getCompName();
          setTimeout( function(){
            window.location.reload();
          },2300);
        }else{
          iziAlert("fa fa-ban","Error!, ","Something was wrong.","bottomLeft","error");
          $("#btn-update").prop("disabled",false);
          $("#btn-update").html("Submit");
        }
      });
    
  });
  

</script>