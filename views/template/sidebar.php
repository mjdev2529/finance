<?php 

  $role = $_SESSION['role'];

  if($role == 0){
    $role = "Administrator";
  }else if($role == 1){
    $role = "Manager";
  }else{
    $role = "Secretary";
  }

  if($page == page_url('home')){
    $active = "active bg-white";
  }elseif ($page == page_url('banks')) {
     $activeBank = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }elseif ($page == page_url('loans')) {
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('add-loan')) {
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('view-loan')) {
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('settings')) {
     $activeGS = "active bg-white";
     $msct = "menu-open";
     $active = "";
  }elseif ($page == page_url('subsidiary')) {
     $activeSUB = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }elseif ($page == page_url('deductions')) {
     $activeGD = "active bg-white";
     $msct = "menu-open";
     $active = "";
  }elseif ($page == page_url('official-receipt')) {
     $activeOR = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('add-official-receipt')) {
     $activeOR = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('loan-print')) {
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('print-official-receipt')) {
     $activeOR = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('forecasting')) {
     $activeFC = "active bg-white";
     $rpt = "menu-open";
     $active = "";
  }elseif ($page == page_url('daily-collection')) {
     $activeDC = "active bg-white";
     $rpt = "menu-open";
     $active = "";
  }elseif ($page == page_url('daily-LOT')) {
     $activeDL = "active bg-white";
     $rpt = "menu-open";
     $active = "";
  }elseif ($page == page_url('users')) {
     $activeUsers = "active bg-white";
     $msct = "menu-open";
     $active = "";
  }elseif ($page == page_url('member-type')) {
     $activeMemType = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }elseif ($page == page_url('borrowers')) {
     $activeMems = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }else{
    $active = "";
  }

?>
<!-- Sidebar user panel (optional) -->
<div class="user-panel row p-3">
  <!-- <div class="image">
    <img src="../assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
  </div> -->
  <div class="col-12 text-center">
    <a href="#" class=""><?php echo ucwords($_SESSION["username"]); ?><br><small class="font-weight-bold">( <?php echo ucwords($role); ?> )</small></a>
  </div>
</div>

<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

    <!-- D A S H B O A R D -->

    <li class="nav-item">
      <a href="index.php?page=<?=page_url('home')?>" class="nav-link <?php echo $active;?>">
        <i class="fa fa-tachometer-alt nav-icon"></i>
        <p>
          Dashboard
          <!-- <span class="right badge badge-danger">New</span> -->
        </p>
      </a>
    </li>

    <!-- D A S H B O A R D END -->

    <!-- T R A N S A C T I O N -->

    <li class="nav-item has-treeview <?php echo $tst;?>">
      <a href="#" class="nav-link">
        <i class="nav-icon far fa-handshake"></i>
        <p>
          Transaction
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="index.php?page=<?=page_url('loans')?>" class="nav-link <?php echo $activeLoan;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>LOT</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="index.php?page=<?=page_url('official-receipt')?>" class="nav-link <?php echo $activeOR;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Official Receipt</p>
          </a>
        </li>
        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Cash Disbursement</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Journal Voucher</p>
          </a>
        </li> -->
      </ul>
    </li>

    <!-- T R A N S A C T I O N END -->

    <!-- R E P O R T -->

    <li class="nav-item has-treeview <?php echo $rpt;?>">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-list"></i>
        <p>
          Report
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('daily-collection')?>" class="nav-link <?php echo $activeDC;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Daily Collection</p>
          </a>
        </li>

        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Daily Disbursement</p>
          </a>
        </li> -->

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('daily-LOT')?>" class="nav-link <?php echo $activeDL;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Daily LOT</p>
          </a>
        </li>
        <!-- CV REGISTER  TREE -->
       <!--  <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon text-sm fas fa-circle"></i>
            <p>
               CV Register
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">

            <li class="nav-item">
              <a href="index.php?page=settings" class="nav-link <?php echo $activeGS;?>">
                <i class="text-sm far fa-circle nav-icon"></i>
                <p>Test</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="index.php?page=deductions" class="nav-link <?php echo $activeGD;?>">
                <i class="text-sm far fa-circle nav-icon"></i>
                <p>Test 1</p>
              </a>
            </li>

          </ul>
        </li> -->
        <!-- CV REGISTER  TREE END -->

       <!--  <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>
              OR/TR Register
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>
              LN Register
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>JV Register</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>
              Trial Balance
              <i class="fas fa-angle-left right"></i>
            </p>
            
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>General Ledger</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Subsidiaries</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Subsidiary Ledger</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Pen Balance Acct.</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Analysis</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Penbank</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Aging</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>
              SSS/GSIS
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>New Accounts</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Reloan/Renew</p>
          </a>
        </li> -->

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('forecasting')?>" class="nav-link <?php echo $activeFC;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Forecasting</p>
          </a>
        </li>

        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Actual</p>
          </a>
        </li> -->

      </ul>
    </li>

    <!-- R E P O R T END -->

    <!-- M A I N T E N A N C E -->

    <li class="nav-item has-treeview <?php echo $mtct;?>">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-tools"></i>
        <p>
          Maintenance
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">

        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Chart of Accounts</p>
          </a>
        </li> -->

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('subsidiary')?>" class="nav-link <?php echo $activeSUB;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Subsidiary</p>
          </a>
        </li>

        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Amort. Cancellation</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Loan Type</p>
          </a>
        </li> -->

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('banks')?>" class="nav-link <?php echo $activeBank;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Bank</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('member-type')?>" class="nav-link <?php echo $activeMemType;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Type of Member</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('borrowers')?>" class="nav-link <?php echo $activeMems;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Borrowers</p>
          </a>
        </li>

      </ul>
    </li>

    <!-- M A I N T E N A N C E END -->

    <!-- M I S C E L L A N E O U S -->

    <li class="nav-item has-treeview <?php echo $msct;?>">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-asterisk"></i>
        <p>
          Miscellaneous
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('settings')?>" class="nav-link <?php echo $activeGS;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Global Settings</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="index.php?page=<?=page_url('deductions')?>" class="nav-link <?php echo $activeGD;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Global Deductions</p>
          </a>
        </li>

        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Back up</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>Restore</p>
          </a>
        </li> -->

        <li class="nav-item">
           <a href="index.php?page=<?=page_url('users')?>" class="nav-link <?php echo $activeUsers;?>">
            <i class="fas fa-circle nav-icon text-sm"></i>
            <p>User Maintenance</p>
          </a>
        </li>

      </ul>
    </li>

    <!-- M I S C E L L A N E O U S END -->

  </ul>
</nav>
<!-- /.sidebar-menu -->