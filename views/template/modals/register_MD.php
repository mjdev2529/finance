<div class="modal fade" id="modal_register" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-user-plus mr-2"></i> Add new User</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">

              <form id="register_form" method="post" class="form-horizontal" action="">
                <div class="card-body">

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">First Name:</label>

                    <div class="col-12">
                      <input type="text" class="form-control" name="fname" placeholder="First Name">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">Last Name:</label>

                    <div class="col-12">
                      <input type="text" class="form-control" name="lname" placeholder="Last Name">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="control-label">Username:</label>

                    <div class="col-12">
                      <input type="text" class="form-control" name="username" placeholder="Username">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">Password:</label>

                    <div class="col-12">
                      <input type="password" class="form-control" id="reg_pass" name="password" placeholder="Password" value="12345" readonly="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">Role:</label>

                    <div class="col-12">
                      <select class="form-control" name="role">
                        <option value="">Please Select:</option>
                        <option value="1">Owner</option>
                        <option value="2">Secretary</option>
                      </select>
                    </div>
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="btn-register" class="btn btn-info btn-flat btn-block">Save</button>
                </div>
                <!-- /.card-footer -->
              </form>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#register_form").submit( function(e){
      e.preventDefault();
      var url = "../ajax/add_user.php";
      var data = $(this).serialize();
      $("#btn-register").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","A new User was added.","bottomLeft","success");
          $("#modal_register").modal("hide");
          $("#btn-register").prop("disabled", false);
          $("input").val("");
          $("select").val("");
          $("#reg_pass").val("12345");
          getUsers();
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-register").prop("disabled", false);
          alert(data);
        }
      });
    });
</script>