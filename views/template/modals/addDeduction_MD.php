<div class="modal fade" id="modal_add_deduction" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-plus mr-2"></i> Add New Deduction</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">

              <form id="deduct_form" method="post" class="form-horizontal" action="">
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-6 control-label">Deduction Name:</label>

                    <div class="col-12">
                      <input type="text" class="form-control" id="d_name" name="d_name" placeholder="Deduction Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-6 control-label">Deduction Value:</label>

                    <div class="col-12">
                      <input type="number" class="form-control" id="d_value" name="d_value" placeholder="Deduction Value">
                    </div>
                  </div>
                  <div class="form-group">

                    <div class="col-6">
                      <label for="inputEmail3" class="control-label mr-3">Set as preset:</label>
                      <input type="checkbox" id="d_preset" value="1">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="btn-add-deduct" class="btn btn-secondary btn-flat btn-block">Save</button>
                </div>
                <!-- /.card-footer -->
              </form>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#deduct_form").submit( function(e){
      e.preventDefault();
      var preset = $("#d_preset").is(":checked")?1:0;
      var url = "../ajax/add_deduct.php";
      var data = $(this).serialize()+"&d_preset="+preset;
      // alert(data);
      $("#btn-add-deduct").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","A new Deduction was added.","bottomLeft","success");
          $("#modal_add_deduction").modal("hide");
          $("#btn-add-deduct").prop("disabled", false);
          $("input").val("");
          deduction();
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-add-deduct").prop("disabled", false);
          alert(data);
        }
      });
    });
</script>