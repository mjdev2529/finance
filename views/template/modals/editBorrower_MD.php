<?php

  include '../../../core/conn.php';

  if(isset($_POST["id"])){
    $id = $_POST["id"];
    $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_borrowers WHERE borrower_id = '$id'"));
    $data1 = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_borrowers_details WHERE borrower_id = '$id'"));
    // $data2 = mysqli_fetch_array(mysqli_query("SELECT * FROM tbl_borrowers_beneficiaries WHERE borrower_id = '$id'"));
  }

?>
<style type="text/css">
  table.dataTable thead > tr > th.tbl-btn {
    width: 15px;
  }
</style>
<div class="modal fade" id="modal_edit_borrowers" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-users mr-2"></i> Add new Borrower</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">

              <form id="editBorrower_form" method="post" class="form-horizontal" action="">
                <input type="hidden" name="b_id" value="<?php echo $id;?>">
                <div class="card-body">
                  <div class="row">

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">BORROWER TYPE:</label>

                      <div class="col-12">
                        <select class="form-control" name="mem_type_id" required="">
                          <option value="">Select:</option>
                          <?php 
                            $mem_type = mysqli_query($conn, "SELECT * FROM tbl_members_type");
                            while($row = mysqli_fetch_array($mem_type)){
                          ?>
                            <option value="<?php echo $row["mem_type_id"];?>" <?php if($data["mem_type_id"] ==  $row["mem_type_id"]){ echo "selected"; }?>><?php echo $row["mem_type_name"];?></option>
                          <?php }?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">CODE:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="code" placeholder="Code" required="" value="<?php echo $data['b_code'];?>">
                      </div>
                    </div>

                    <div class="form-group col-2"></div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">FIRST NAME:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="fname" placeholder="First Name" required="" value="<?php echo $data['b_fname'];?>">
                      </div>
                    </div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">LAST NAME:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="lname" placeholder="Last Name" required="" value="<?php echo $data['b_lname'];?>">
                      </div>
                    </div>

                    <div class="form-group col-12">
                      <label for="inputPassword3" class="control-label">ADDRESS:</label>

                      <div class="col-12">
                        <textarea class="form-control" placeholder="Type Here..." name="address" required=""><?php echo $data["b_address"];?></textarea>
                      </div>
                    </div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">TELEPHONE #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="telno" placeholder="Telephone #" value="<?php echo $data['b_telno'];?>">
                      </div>
                    </div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">PHONE #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="celno" placeholder="Phone #" value="<?php echo $data['b_phoneno'];?>">
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">BIRTHDATE:</label>

                      <div class="col-12">
                        <input type="date" class="form-control" name="bdate" required="" oninput="getAge()" value="<?php echo date('Y-m-d', strtotime($data['b_bdate']));?>">
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">AGE:</label>

                      <div class="col-12">
                        <input type="number" class="form-control" name="age" placeholder="Age" min="1" required="" value="<?php echo $data['b_age']?>">
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">CIVIL STATUS:</label>

                      <div class="col-12">
                        <select class="form-control" name="status" required="">
                          <option <?php if($data["b_civilstatus"] == "S"){ echo "selected";}?> value="S">SINGLE</option>
                          <option <?php if($data["b_civilstatus"] == "M"){ echo "selected";}?> value="M">MARRIED</option>
                        </select>
                      </div>
                    </div>

                     <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">SSS #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="sss_num" placeholder="SSS #" value="<?php echo $data1['sss_num'];?>">
                        <input type="hidden" name="member_id" value="<?php echo $id;?>">
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">GSIS #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="gsis_num" placeholder="GSIS #" value="<?php echo $data1['gsis_num'];?>">
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">PHIL. HEALTH #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="ph_num" placeholder="PHIL. HEALTH #" value="<?php echo $data1['ph_num'];?>">
                      </div>
                    </div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">AMOUNT OF PENSION:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="pen_amount" placeholder="AMOUNT OF PENSION" value="<?php echo $data1['pen_amount'];?>">
                      </div>
                    </div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">SOURCES OF PENSION:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="pen_source" placeholder="SOURCES OF PENSION" value="<?php echo $data1['pen_source'];?>">
                      </div>
                    </div>

                    <div class="form-group col-12">
                      <label for="inputPassword3" class="control-label">BENEFICIARIES:</label>
                    </div>

                    <div class="form-group col-6">
                      <input type="text" class="form-control" id="ben_name" placeholder="NAME">
                    </div>

                    <div class="form-group col-2">
                      <input type="text" class="form-control" id="ben_age" placeholder="AGE">
                    </div>

                    <div class="col-2">
                      <button type="button" id="btn-add-ben" onclick="addBeneficiary()" class="btn btn-secondary btn-flat">ADD</button>
                    </div>

                    <div class="col-12">
                      <table width="100%" id="tbl_mem_beneficiary" class="table table-bordered table-striped">
                        <thead class="bg-dark">
                        <tr>
                          <th width="5px;">#</th>
                          <th class="tbl-btn"></th>
                          <th>Name</th>
                          <th width="15px;">Age</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>

                    <div class="dropdown-divider col-12"></div>

                      <!-- B E N E F I C I A R I E S  E N D -->

                    <div class="form-group col-12">
                      <label for="inputPassword3" class="control-label">REFERRED BY:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="ref_by" placeholder="REFERRED BY" value="<?php echo $data1['ref_by'];?>">
                      </div>
                    </div>

                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="btn-register" class="btn btn-secondary btn-flat col-6 offset-3">Save</button>
                </div>
                <!-- /.card-footer -->
              </form>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $(document).ready( function(){
    var b_id = "<?php echo $id;?>";
    get_Beneficiaries(b_id);
  });

  function addBeneficiary(){
    var b_id = $("input[name=member_id]").val();
    var ben_name = $("#ben_name").val();
    var ben_age = $("#ben_age").val();
    var url = "../ajax/add_beneficiary.php";
    $("#btn-add-ben").prop("disabled", true);
    $.post(url,{b_id: b_id,ben_name: ben_name, ben_age: ben_age}, function(data){
      if(data != null){
        iziAlert("fa fa-check","Success! ,","A new beneficiary was added.","bottomLeft","success");
        $("#btn-add-ben").prop("disabled", false);
        $("#ben_name").val("");
        $("#ben_age").val("");
        get_Beneficiaries(data);
      }else{
        iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
        alert(data);
        $("#btn-add-ben").prop("disabled", false);
      }
    });
  }

  function delete_beneficiary(id){
    var x = confirm("Are you sure to delete?");
    if(x){
      $(".btn-del").prop("disabled", true);
      var url = "../ajax/delete_beneficiary.php";
      $.post(url,{b_id: id}, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","Selected beneficiary was deleted.","bottomLeft","success");
          $(".btn-del").prop("disabled", false);
          get_Beneficiaries("<?php echo $id;?>");
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          alert(data);
          $(".btn-del").prop("disabled", false);

        }
      });
    }
  }

  function get_Beneficiaries(id){
    $("#tbl_mem_beneficiary").DataTable().destroy();
    $('#tbl_mem_beneficiary').dataTable({
      "processing":true,
      "order": [[ 1, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/beneficiaries.php",
        "data":{
          mem_id:id
        }
      },
      "columns":[
        {
          "data":"count"
        },
        {
          "mRender": function(data,type,row){
            
            return "<button type='button' class='btn btn-sm btn-flat btn-danger btn-del' onclick='delete_beneficiary("+row.ben_id+")'><i class='fa fa-trash'></i></button>";
          }
        },
        {
          "data":"ben_name"
        },
        {
          "data":"ben_age"
        }
      ]
    });
  }

  function getAge(){
    var enteredDate = $("input[name=bdate]").val();
    var years = new Date(new Date() - new Date(enteredDate)).getFullYear() - 1970;
    $("input[name=age]").val(years);
  }

  $("#editBorrower_form").submit( function(e){
      e.preventDefault();
      var url = "../ajax/update_borrowers.php";
      var data = $(this).serialize();
      $("#btn-register").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","Borrower details was updated.","bottomLeft","success");
          $("#modal_edit_borrowers").modal("hide");
          $("#btn-register").prop("disabled", false);
          getMembers();
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-register").prop("disabled", false);
          alert(data);
        }
      });
    });
</script>