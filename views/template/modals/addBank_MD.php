<div class="modal fade" id="modal_add_bank" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-plus mr-2"></i> Add New Bank</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">

              <form id="addBank_form" method="post" class="form-horizontal" action="">
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-6 control-label">Bank Name:</label>

                    <div class="col-12">
                      <input type="text" class="form-control" id="b_name" name="b_name" placeholder="Bank Name">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="btn-add-bank" class="btn btn-secondary btn-flat btn-block">Save</button>
                </div>
                <!-- /.card-footer -->
              </form>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#addBank_form").submit( function(e){
      e.preventDefault();
      var url = "../ajax/add_bank.php";
      var data = $(this).serialize();
      $("#btn-add-bank").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","A new add Bank was added.","bottomLeft","success");
          $("#modal_add_bank").modal("hide");
          $("#b_name").val("");
          $("#btn-add-bank").prop("disabled", false);
          get_Allbanks();
          
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-add-bank").prop("disabled", false);
          alert(data);
        }
      });
    });
</script>