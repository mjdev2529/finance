<div class="modal fade" id="modal_edit_bank" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-edit mr-2"></i> Edit Bank Details</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="editBank_form" method="post" class="form-horizontal" action="">
              </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#editBank_form").submit( function(e){
      e.preventDefault();
      var url = "../ajax/update_bank.php";
      var data = $(this).serialize();
      $("#btn-edit-bank").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","A new User was added.","bottomLeft","success");
          $("#modal_edit_bank").modal("hide");
          $("#btn-edit-bank").prop("disabled", false);
          get_Allbanks();
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-edit-bank").prop("disabled", false);
          alert(data);
        }
      });
    });
</script>