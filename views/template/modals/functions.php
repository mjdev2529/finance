<?php

 function enCrypt($data){
 	return base64_encode($data);
 }

 function deCrypt($data){
 	return base64_decode($data);
 }

 function roomType($id){
 	global $con;
 	$room = mysqli_fetch_array(mysqli_query($con,"SELECT room_name FROM room_type WHERE room_type_id = '$id'"));
 	$name = $room[0];
 	return $name;
 }

 function beddingType($id){
 	global $con;
 	$bedding = mysqli_fetch_array(mysqli_query($con,"SELECT bedding_name FROM bedding_type WHERE bedding_id = '$id'"));
 	$name = $bedding[0];
 	return $name;
 }

function userlogs($id,$log){
    global $con;
    $date = date("Y-m-d H:i:s");
    $insert_logs = mysqli_query($con,"INSERT INTO user_logs SET user_id = '$id', activity = '$log', date_added = '$date'");
    if($insert_logs){
        return true;
    }else{
        return "Error Logs";
    }
}

 function itexmo($number,$message){
 	global $itexmo,$param,$context,$url;
	//API CODE HERE
	$apicode = "TR-MMXXV250522_G25H6";

	$url = 'https://www.itexmo.com/php_api/api.php';

	$itexmo = array('1' => $number, '2' => $message, '3' => $apicode);

	$param = array(

	    'http' => array(

	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",

	        'method'  => 'POST',

	        'content' => http_build_query($itexmo),

	    ),);

	$context  = stream_context_create($param);

	return file_get_contents($url, false, $context);

}


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

?>