<div class="modal fade" id="modal_confirm" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-exclamation-triangle mr-2"></i>Enter Password to confirm action.</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">

              <form id="confirm_form" method="post" class="form-horizontal" action="">
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Username:</label>
                    <div class="col-12">
                      <input type="hidden" class="form-control" id="user_id" name="id">
                      <input type="text" class="form-control" name="username" placeholder="Username" required="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password:</label>
                    <div class="col-12">
                      <input type="password" class="form-control" name="password" placeholder="Password" required="">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="btn-confirm" class="btn btn-info btn-flat btn-block">Confirm</button>
                </div>
                <!-- /.card-footer -->
              </form>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#confirm_form").submit( function(e){
      e.preventDefault();

      var url = "../ajax/delete_user.php";
      var data = $(this).serialize();

      $("#btn-confirm").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","A new User was added.","bottomLeft","success");
          $("#modal_confirm").modal("hide");
          $("#btn-confirm").prop("disabled", false);
          $("input").val("");
          $("#reg_pass").val("12345");
          getUsers();
        }else if(data == 2){
          iziAlert("fa fa-ban","Warning! ,","You don't have privilege for this action.","bottomLeft","warning");
          $("#btn-confirm").prop("disabled", false);
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-confirm").prop("disabled", false);
          alert(data);
        }
      });
      
    });
</script>