<div class="modal fade" id="modal_add_borrowers" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-users mr-2"></i> Add new Borrower</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="card-body">

                  <form id="addBorrower_form" method="post" class="form-horizontal" action="">
                      <div class="row">

                        <div class="form-group col-6">
                          <label for="inputPassword3" class="control-label">BORROWER TYPE:</label>

                          <div class="col-12">
                            <select class="form-control" name="mem_type_id" required="">
                              <option value="">Select:</option>
                              <?php 
                                $mem_type = mysqli_query($conn, "SELECT * FROM tbl_members_type");
                                while($row = mysqli_fetch_array($mem_type)){
                              ?>
                                <option value="<?php echo $row["mem_type_id"];?>"><?php echo $row["mem_type_name"];?></option>
                              <?php }?>
                            </select>
                          </div>
                        </div>

                        <div class="form-group col-4">
                          <label for="inputPassword3" class="control-label">CODE:</label>

                          <div class="col-12">
                            <input type="text" class="form-control" name="code" placeholder="Code" required="">
                          </div>
                        </div>

                        <div class="form-group col-2"></div>

                        <div class="form-group col-6">
                          <label for="inputPassword3" class="control-label">FIRST NAME:</label>

                          <div class="col-12">
                            <input type="text" class="form-control" name="fname" placeholder="First Name" required="">
                          </div>
                        </div>

                        <div class="form-group col-6">
                          <label for="inputPassword3" class="control-label">LAST NAME:</label>

                          <div class="col-12">
                            <input type="text" class="form-control" name="lname" placeholder="Last Name" required="">
                          </div>
                        </div>

                        <div class="form-group col-12">
                          <label for="inputPassword3" class="control-label">ADDRESS:</label>

                          <div class="col-12">
                            <textarea class="form-control" placeholder="Type Here..." name="address" required=""></textarea>
                          </div>
                        </div>

                        <div class="form-group col-6">
                          <label for="inputPassword3" class="control-label">TELEPHONE #:</label>

                          <div class="col-12">
                            <input type="text" class="form-control" name="telno" placeholder="Telephone #">
                          </div>
                        </div>

                        <div class="form-group col-6">
                          <label for="inputPassword3" class="control-label">PHONE #:</label>

                          <div class="col-12">
                            <input type="text" class="form-control" name="celno" placeholder="Phone #">
                          </div>
                        </div>

                        <div class="form-group col-4">
                          <label for="inputPassword3" class="control-label">BIRTHDATE:</label>

                          <div class="col-12">
                            <input type="date" class="form-control" oninput="getAge()" name="bdate" required="">
                          </div>
                        </div>

                        <div class="form-group col-4">
                          <label for="inputPassword3" class="control-label">AGE:</label>

                          <div class="col-12">
                            <input type="number" class="form-control" name="age" placeholder="Age" min="1" required="">
                          </div>
                        </div>

                        <div class="form-group col-4">
                          <label for="inputPassword3" class="control-label">CIVIL STATUS:</label>

                          <div class="col-12">
                            <select class="form-control" name="status" required="">
                              <option value="S">SINGLE</option>
                              <option value="M">MARRIED</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 row">
                        <button type="submit" id="btn-add-mem" class="btn btn-secondary btn-flat col-2 offset-10">Add</button>
                      </div>
                  </form>

                  <div class="b_details" style="display: none;">
                  	<form id="add_borrowerDetails_form" method="post" class="row">
                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">SSS #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="sss_num" placeholder="SSS #">
                        <input type="hidden" name="member_id">
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">GSIS #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="gsis_num" placeholder="GSIS #">
                      </div>
                    </div>

                    <div class="form-group col-4">
                      <label for="inputPassword3" class="control-label">PHIL. HEALTH #:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="ph_num" placeholder="PHIL. HEALTH #">
                      </div>
                    </div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">AMOUNT OF PENSION:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="pen_amount" placeholder="AMOUNT OF PENSION">
                      </div>
                    </div>

                    <div class="form-group col-6">
                      <label for="inputPassword3" class="control-label">SOURCES OF PENSION:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="pen_source" placeholder="SOURCES OF PENSION">
                      </div>
                    </div>

                    <div class="form-group col-12">
                      <label for="inputPassword3" class="control-label">BENEFICIARIES:</label>
                    </div>

                    <div class="form-group col-6">
                      <input type="text" class="form-control" id="ben_name" placeholder="NAME">
                    </div>

                    <div class="form-group col-2">
                      <input type="text" class="form-control" id="ben_age" placeholder="AGE">
                    </div>

                    <div class="col-2">
                      <button type="button" id="btn-add-ben" onclick="addBeneficiary()" class="btn btn-secondary btn-flat">ADD</button>
                    </div>

                    <div class="col-12">
                      <table id="tbl_mem_beneficiary" class="table table-bordered table-striped">
                        <thead class="bg-dark">
                        <tr>
                          <th width="15px">#</th>
                          <th style="width:15px !important;"></th>
                          <th>Name</th>
                          <th width="25px">Age</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>

                    <div class="dropdown-divider col-12"></div>

                    <div class="form-group col-12">
                      <label for="inputPassword3" class="control-label">REFERRED BY:</label>

                      <div class="col-12">
                        <input type="text" class="form-control" name="ref_by" placeholder="REFERRED BY">
                      </div>
                    </div>

                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer b_details" style="display: none;">
                  <button type="submit" id="btn-register" class="btn btn-info btn-flat col-6 offset-3">Save</button>
                </div>
                <!-- /.card-footer -->
              </form>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">

  $(document).ready( function(){
    get_Beneficiaries(0);
  });

  function addBeneficiary(){
  	var b_id = $("input[name=member_id]").val();
  	var ben_name = $("#ben_name").val();
  	var ben_age = $("#ben_age").val();
  	var url = "../ajax/add_beneficiary.php";
  	$("#btn-add-ben").prop("disabled", true);
  	$.post(url,{b_id: b_id,ben_name: ben_name, ben_age: ben_age}, function(data){
  		if(data != null){
  			iziAlert("fa fa-check","Success! ,","A new beneficiary was added.","bottomLeft","success");
  			$("#btn-add-ben").prop("disabled", false);
		  	$("#ben_name").val("");
		  	$("#ben_age").val("");
		  	get_Beneficiaries(data);
  		}else{
  			iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
  			alert(data);
  			$("#btn-add-ben").prop("disabled", false);
  		}
  	});
  }

  function get_Beneficiaries(id){
    $("#tbl_mem_beneficiary").DataTable().destroy();
    $('#tbl_mem_beneficiary').dataTable({
      "processing":true,
      "order": [[ 1, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/beneficiaries.php",
        "data":{
          mem_id:id
        }
      },
      "columns":[
        {
          "data":"count"
        },
        {
          "mRender": function(data,type,row){
            
            return "<button type='button' class='btn btn-sm btn-flat btn-danger btn-del' onclick='delete_beneficiary("+row.ben_id+")'><i class='fa fa-trash'></i></button>";
          }
        },
        {
          "data":"ben_name"
        },
        {
          "data":"ben_age"
        }
      ]
    });
  }

  function delete_beneficiary(id){
  	var x = confirm("Are you sure to delete?");
    if(x){
    	$(".btn-del").prop("disabled", true);
	    var url = "../ajax/delete_beneficiary.php";
	    $.post(url,{b_id: id}, function(data){
	      if(data == 1){
	        iziAlert("fa fa-check","Success! ,","Selected beneficiary was deleted.","bottomLeft","success");
	        $(".btn-del").prop("disabled", false);
    		  get_Beneficiaries(id);
	      }else{
	        iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
	        alert(data);
	        $(".btn-del").prop("disabled", false);

	      }
	    });
    }
  }

  function getAge(){
    var enteredDate = $("input[name=bdate]").val();
    var years = new Date(new Date() - new Date(enteredDate)).getFullYear() - 1970;
    $("input[name=age]").val(years);
  }

  $("#addBorrower_form").submit( function(e){
      e.preventDefault();
      var url = "../ajax/add_borrowers.php";
      var data = $(this).serialize();
      $("#btn-add-mem").prop("disabled", true);
      $.post(url, data, function(data){
        if(data != ""){
          iziAlert("fa fa-check","Success! ,","A new Borrower was added.","bottomLeft","success");
          $(".b_details").show();
          $("input[name=member_id]").val(data);
          // $("#modal_add_borrowers").modal("hide");
          $("#btn-add-mem").prop("disabled", false);
          $("#btn-add-mem").hide();
          // $("input").val("");
          // $("select").val("");
          // $("textarea").val("");
          // getMembers();
          get_Beneficiaries(data);
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-add-mem").prop("disabled", false);
          alert(data);
        }
      });
    });

  $("#add_borrowerDetails_form").submit( function(e){
      e.preventDefault();
      var url = "../ajax/add_borrower_details.php";
      var data = $(this).serialize();
      $("#btn-add-mem").prop("disabled", true);
      $.post(url, data, function(data){
        if(data != ""){
          iziAlert("fa fa-check","Success! ,","Borrower details was added.","bottomLeft","success");
          $("#modal_add_borrowers").modal("hide");
          $("#btn-register").prop("disabled", false);
          $("input").val("");
          $("select").val("");
          $("textarea").val("");
          $("hidden").val("");
          $(".b_details").hide();
          $("#btn-add-mem").show();
          getMembers();
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-register").prop("disabled", false);
          alert(data);
        }
      });
    });

</script>