<div class="modal fade" id="modal_edit_deduct" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-edit mr-2"></i> Edit Deduction Details</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="editDeduction_form" method="post" class="form-horizontal" action="">
              </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#editDeduction_form").submit( function(e){
      e.preventDefault();
      var preset = $("#d_preset").is(":checked")?1:0;
      var url = "../ajax/update_deduction.php";
      var data = $(this).serialize()+"&d_preset="+preset;
      $("#btn-edit-deduct").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","Deduction details was updated.","bottomLeft","success");
          $("#modal_edit_bank").modal("hide");
          $("#btn-edit-deduct").prop("disabled", false);
          $("#modal_edit_deduct").modal("hide");
          deduction();
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-edit-deduct").prop("disabled", false);
          alert(data);
        }
      });
    });
</script>