<?php

  include '../../../core/conn.php';

  if(isset($_POST["id"])){
    $id = $_POST["id"];
    $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_members_type WHERE mem_type_id = '$id'"));
  }

?>
<div class="modal fade" id="modal_edit_memType" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-edit mr-2"></i> Edit new Member Type</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="editMemType_form" method="post" class="form-horizontal" action="">
                <div class="card-body">

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">Member Type:</label>

                    <div class="col-12">
                      <input type="text" class="form-control" name="mem_type" placeholder="Member Type" value="<?php echo $data['mem_type_name']?>">
                      <input type="hidden" name="mem_type_id" value="<?php echo $data['mem_type_id']?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">Collection Date (Every month):</label>

                    <div class="row">
                      <div class="input-group mb-3 col-6">
                      <div class="input-group-prepend">
                        <span class="input-group-text">1.</span>
                      </div>
                      <input type="number" class="form-control" min="0" max="31" name="mem_coll_date" value="<?php echo $data['mem_coll_date']?>">
                    </div>
                    <div class="input-group mb-3 col-6">
                      <div class="input-group-prepend">
                        <span class="input-group-text">2.</span>
                      </div>
                      <input type="number" class="form-control" min="0" max="31" name="mem_coll_date2" value="<?php echo $data['mem_coll_date2']?>">
                    </div>
                    </div>
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="btn-edit" class="btn btn-info btn-flat btn-block">Save</button>
                </div>
                <!-- /.card-footer -->
              </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#editMemType_form").submit( function(e){
        var num1 = $("#mem_coll_date").val();
        var num2 = $("#mem_coll_date2").val();
        if(num1==0 && num2==0){
        e.preventDefault();
        var url = "../ajax/update_memType.php";
        var data = $(this).serialize();
        $("#btn-edit").prop("disabled", true);
        $.post(url, data, function(data){
          if(data == 1){
            iziAlert("fa fa-check","Success! ,","Member Type details was updated.","bottomLeft","success");
            $("#modal_edit_memType").modal("hide");
            $("#btn-edit").prop("disabled", false);
            $("input").val("");
            getType();
          }else{
            iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
            $("#btn-edit").prop("disabled", false);
            alert(data);
          }
        });
      }
    });
</script>