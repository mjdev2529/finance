<div class="modal fade" id="modal_add_MemType" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-users mr-2"></i> Add new Member Type</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">

              <form id="addMemType_form" method="post" class="form-horizontal" action="">
                <div class="card-body">

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">Member Type:</label>

                    <div class="col-12">
                      <input type="text" class="form-control" name="mem_type" placeholder="Member Type">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="control-label">Collection Date (Every month):</label>

                    <div class="row">
                    	<div class="input-group mb-3 col-6">
		                  <div class="input-group-prepend">
		                    <span class="input-group-text">1.</span>
		                  </div>
		                  <input type="number" class="form-control" min="0" max="31" name="mem_coll_date" id ="mem_coll_date">
		                </div>
		                <div class="input-group mb-3 col-6">
		                  <div class="input-group-prepend">
		                    <span class="input-group-text">2.</span>
		                  </div>
		                  <input type="number" class="form-control" min="0" max="31" name="mem_coll_date2" id ="mem_coll_date2">
		                </div>
                    </div>
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" id="btn-register" class="btn btn-secondary btn-flat btn-block">Save</button>
                </div>
                <!-- /.card-footer -->
              </form>

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
  $("#addMemType_form").submit( function(e){
      var num1 = $("#mem_coll_date").val();
      var num2 = $("#mem_coll_date2").val();
      if(num1==0 && num2==0){
        alert("Please fill in required fields");
      }else{
          e.preventDefault();
      var url = "../ajax/add_MemType.php";
      var data = $(this).serialize();
      $("#btn-register").prop("disabled", true);
      $.post(url, data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success! ,","A new Member Type was added.","bottomLeft","success");
          $("#modal_add_MemType").modal("hide");
          $("#btn-register").prop("disabled", false);
          $("input").val("");
          getType();
        }else{
          iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
          $("#btn-register").prop("disabled", false);
          alert(data);
        }
      });
      }
    


    });
</script>