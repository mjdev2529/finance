<style type="text/css">
  .modal-xl {
    max-width: 98% !important;
    margin-left: 2% !important;
  }
</style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Loans</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Loans</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <div class="float-right mb-2">
              <button class="btn btn-secondary btn-sm" onclick="add_loan()"><i class="fa fa-plus"></i> Add</button>
              <button class="btn btn-danger btn-sm" onclick="delete_loan();"><i class="fa fa-trash"></i> Delete</button>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                   <div class="col-sm-12 mb-2">
                  <div class="row">
                    
                    <div class="col-sm-4 offset-sm-1">
                      <span>Start Date:</span>
                      <input id="start_date" type="date" class="form-control" value="<?php  echo get_first_day_current_month();?>">
                    </div>
                    <div class="col-sm-4">
                      <span>End Date:</span>
                      <input id="end_date" type="date" class="form-control" value="<?php echo get_last_day_current_month();?>">
                    </div>
                    <div class="col-sm-2 pt-4">
                      <button type="button" href="#" class="btn btn-secondary" onclick="get_data()"><i class="fas fa-sync-alt"></i> Generate</button>
                    </div>
              </div>
                </div>

                <div class="dropdown-divider col-md-12 mt-3 mb-3"></div>

                <table width="100%" id="tbl_loans" class="table table-sm table-bordered table-striped">
                  <thead>
                  <tr class="bg-dark">
                    <th width="20px;"><input type="checkbox" id="checkAll" onchange="checkAll()"></th>
                    <th width="20px;">#</th>
                    <th width="10px;"></th>
                    <th width="100px;">Loan #</th>
                    <th width="150px;">Date</th>
                    <th>Pensioner</th>
                    <th width="110">Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>                
                </table>
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
<?php require "template/modals/viewLoans_MD.php";?>
<?php //require "template/modals/add_loans.php";?>
<script type="text/javascript">

  $(document).ready( function(){
    $("#tbl_loans").dataTable();
    get_data();
  });

  function add_loan(){
    window.location="index.php?page=add-loan";
  }

  function checkAll(){
    var x = $("#checkAll").is(":checked");
    if(x){
      $("input[name=loans]").prop("checked", true);
    }else{
      $("input[name=loans]").prop("checked", false);
    }
  }

  function viewLoan(id){
    var url = "../ajax/loan_details.php";
    $.post(url,{id: id},function(data){
      $(".loan_body").html(data);
      $("#modal_view_loans").modal();
    });
  }

  function delete_loan(){

    var url = "../ajax/delete_loan.php";
    var x  = confirm("Are you sure to delete?");
    var checkedVals = [];

    $('input[name=loans]:checked').map(function() {
      checkedVals.push($(this).val());
    });

    if(x){
      if(checkedVals){
        $.post(url,{id: checkedVals},function(data){
          if(data == 1){
            iziAlert("fa fa-check","Success! ,","Selected loan deleted.","bottomLeft","success");
            get_data();
          }else{
           iziAlert("fa fa-ban","Error! ,","Something was wrong.","bottomLeft","error");
          }
        });
      }else{
        iziAlert("fa fa-info","Warning! ,","No loan selected.","bottomLeft","warning");
      }
    }
   
    }

  function get_data(){
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();

    $("#tbl_loans").DataTable().destroy();
    $('#tbl_loans').DataTable({
      "processing":true,
      "order": [[ 1, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/loans.php",
        "dataSrc":"data",
        "data":{
          start_date:start_date,
          end_date:end_date
        }
      },
      "columnDefs": [
        { targets: [0,2], orderable: false}
      ],
      "columns":[
        {
          "mRender": function(data,type,row){
            return "<input type='checkbox' name='loans' value='"+row.id+"'>";
          }
        },
        {
          "data":"count"
        },
        {
          "mRender": function(data,type,row){
            
            if(row.stat != 2){
              return "<div class='dropdown'>"+
                      "<a href='#' class='ml-1 text-center' data-toggle='dropdown'><i class='fa fa-cog text-dark'></i></a>"+
                        "<div class='dropdown-menu'>"+
                        // "<a class='dropdown-item text-sm' href='#'><i class='fa fa-search text-dark'></i> View Details</a>"+
                        "<a class='dropdown-item text-sm' href='index.php?page=<?=page_url('view-loan')?>&loan="+row.id+"'><i class='fa fa-edit text-dark'></i> View Details</a>"+
                        "<div class='divider'></div>"+
                        "<a class='dropdown-item text-sm' href='index.php?page=<?=page_url('loan-print')?>&id="+row.id+"'><i class='fa fa-print text-dark'></i> Print</a>"+
                      "</div>"+
                    "</div>";
            }else{
              return  "<i class='fa fa-ban'></i>";
            }
          }
        },
        {
          "data":"loan_no"
        },
        {
          "data":"date"
        },
        {
          "data":"pensioneers_name"
        }
        ,
        {
          "data":"status"
        }
      ]
    });
  }
</script>