<?php
  $now = date("Y-m-d");

?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Type of Members</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Type of Members</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <div class="row mb-2">
        <div class="col-12">
          <div class="float-sm-right">
            <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal_add_MemType"><i class="fa fa-plus"></i> Add</button>
            <button class="btn btn-danger btn-sm" onclick="deleteMemType()"><i class="fa fa-trash"></i> Delete</button>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table width="100%" id="tbl_users" class="table table-sm table-bordered table-striped">
                <thead>
                <tr class="bg-dark">
                  <th width="15px"><input type="checkbox" id="checkAll" onclick="checkAll()"></th>
                  <th width="15px">#</th>
                  <th width="50px"></th>
                  <th>Type</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
    <div id="editMD"></div>
<?php include "template/modals/addMemType_MD.php"; ?>
<script type="text/javascript">
  $(document).ready( function(){
    getType();
  });

  function getType(){
    $("#tbl_users").DataTable().destroy();
    $("#tbl_users").dataTable({
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/members_type.php",
        "data":"",
        "processing":true
      },
      "order":[1,"asc"],
      "columnDefs":[{targets:[0,2],orderable: false}],
      "columns":[
      {
        "mRender": function(data,type,row){
          return "<input type='checkbox' name='member_type' value='"+row.type_id+"'>";
        }
      },
      {
        "data":"count"
      },
      {
        "mRender": function(data,type,row){
          return "<a href='#' aria-expanded='false' class='btn btn-outline-dark btn-sm' onclick='editMemType("+row.type_id+")'><i class='fa fa-edit mr-1'></i> Edit</a>";
        }
      },
      {
        "data":"name"
      }
      ]
    });
  }

  function checkAll(){
    var x = $("#checkAll").is(":checked");
    if(x){
      $("input[name=member_type]").prop("checked", true);
    }else{
      $("input[name=member_type]").prop("checked", false);
    }
  }

  function editMemType(type_id){
    var url = "template/modals/editMemType_MD.php";
    $.post(url,{id: type_id}, function(data){
      if(data){
        $("#editMD").html(data);
        $("#modal_edit_memType").modal();
      }
    });
  }

  function deleteMemType(){

    allVals = [];
    $("input[name=member_type]:checked").each( function(){
      allVals.push($(this).val());
    });
    url = "../ajax/delete_mem_type.php";

    if(allVals != ""){
      var x = confirm("Are you sure to delete selected member type?");
      $.post(url,{id: allVals}, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Success!","Member Type was successfully deleted.","bottomLeft","success");
          getType();
        }else{
          iziAlert("fa fa-times","Error!","Something was wrong.","bottomLeft","error");
          alert(data);
        }
      });
    }else{
      iziAlert("fa fa-info","Warning!","No Data Selected.","bottomLeft","warning");
    }
  }

</script>