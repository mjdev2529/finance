
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Daily Collection</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item active">Daily Collection</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                    <center class="loader" style="display: none;">
                      <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                      <br>
                      Loading please wait...
                    </center>
                     <?php 
                            $date = date("Y-m-d");
                            $or_sql = mysqli_query($conn, "SELECT * FROM tbl_official_receipt WHERE date_added = '$date'");
                            $checker = mysqli_num_rows($or_sql);
                            if($checker > 0){
                      ?>
                    <table id="tbl_daily_collection" class="table table-sm table-stripped mt-2" style="display: none;">
                      <thead class="bg-dark">
                        <th>#</th>
                        <th>OR #</th>
                        <th>Date</th>
                        <th>Pensioneer</th>
                        <th>Amount Paid</th>
                      </thead>
                      <tbody>
                        <?php
                            $result = mysqli_query($conn, "SELECT * FROM tbl_official_receipt WHERE date_added = '$date'");
                            $count = 1;
                            $x = 0;
                            while($row = mysqli_fetch_array($result)){
                            $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_subsidiary WHERE subsidiary_id = '$row[subsidiary_id]'"));
                            $sum = $data["principal"] + $data["interest"];
                            $x += $sum;
                        ?>
                        <tr>
                          <td><?php echo $count++; ?>.</td>
                          <td><?php echo $row["or_num"]; ?></td>
                          <td><?php echo date("F d, Y", strtotime($row["date_added"])); ?></td>
                          <td><?php echo get_pensioneer_name($data["pensioneer_id"]); ?></td>
                          <td><?php echo number_format($data["principal"] + $data["interest"],2); ?></td>
                        </tr>
                        <?php } ?>
                          <tr style="border-top: 2px solid;">
                            <td colspan="4"><b>TOTAL:</b></td>
                            <td><?php echo number_format($x,2) ?></td>
                          </tr>
                      </tbody>
                    </table>
                <?php }else{?>
                	<h4 class="text-center inform" style="display: none;"><i class="fa fa-info-circle"></i>  No Collection for today.</h4>
                <?php }?>
                  </div>
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
    	$(document).ready( function(){
    		load();
    	});
      function load(){
        $(".loader").show();
        setTimeout( function(){
          $(".loader").hide();
          $("#tbl_daily_collection").show();
          $(".inform").show();
        },1500);
      }
    </script>