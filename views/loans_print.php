<?php
  $id = $_GET["id"];
  $data = mysqli_fetch_array(mysqli_query($conn, "SELECT loan_no FROM tbl_loan WHERE loan_id = '$id'"));
?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Loans Print <small>( Loan #: <?php echo $data[0]; ?> )</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('loans')?>">Loans</a></li>
          <li class="breadcrumb-item active">Loans Print</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="btn-group float-right mb-3">
              <button type="button" class="btn btn-secondary" onclick="printFrame()"><i class="fas fa-print"></i> Print</button>
            </div>
            <div class="dropdown-divider col-md-12"></div>
            <iframe id="loan_frame" src="print/loans.php?id=<?php echo $id;?>" width="100%" style="height: 1344px;"></iframe>
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </div><!--/. container-fluid -->
</section>
<!-- /.content -->

<script type="text/javascript">
	function printFrame() {
            var frm = document.getElementById("loan_frame").contentWindow;
            frm.focus();// focus on contentWindow is needed on some ie versions
            frm.print();
            return false;
	}
</script>