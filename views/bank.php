<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Banks</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Banks</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">

	<div class="row mb-2">
	  <div class="col-12">
	  	<div class="float-right">
	      <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal_add_bank"><i class="fas fa-plus-circle"></i> Add</button>
	      <button type="button" class="btn btn-danger btn-sm" onclick="delete_bank();"><i class="fa fa-trash"></i> Delete</button>
	    </div>
	  </div>
	</div>

	<div class="row">
	  <div class="col-12">
	    <div class="card">
	      <div class="card-body">
	        <table id="tbl_bank" class="table table-sm table-bordered table-striped">
	          <thead>
	          <tr class="bg-dark">
	            <th width="20px;"><input type="checkbox" id="checkAll" onchange="checkAll()"></th>
	            <th width="20px;">#</th>
	            <th width="50px;"></th>
	            <th>Bank Name</th>
	          </tr>
	          </thead>
	          <tbody>
	           
	          </tbody>                
	        </table>
	      </div>
	      <!-- /.card -->
	    </div>
	  </div>
	  <!-- /.col -->
	</div>
    <!-- /.row -->
</section>
<!-- /.content -->

<?php include "template/modals/addBank_MD.php";?>
<?php include "template/modals/editBank_MD.php";?>

<script type="text/javascript">
  $(document).ready( function(){
    get_Allbanks();
  });

  function checkAll(){
    var cb = $("#checkAll").is(":checked");
    if(cb){
      $("input[name='cb_bank']").prop("checked", true);
    }else{
      $("input[name='cb_bank']").prop("checked", false);
    }
  }

  function delete_bank(){

    var url = "../ajax/delete_bank.php";
    var x  = confirm("Are you sure to delete?");
    var checkedVals = [];

    $('input[name=cb_bank]:checked').map(function() {
      checkedVals.push($(this).val());
    });

    if(x){
      if(checkedVals != ""){
        $.post(url,{id: checkedVals},function(data){
          if(data == 1){
            iziAlert("fa fa-check","Success! ,","Selected bank deleted.","bottomLeft","success");
            get_Allbanks();
          }else{
           iziAlert("fa fa-ban","Error! ,","Something was wrong.","bottomLeft","error");
          }
        });
      }else{
        iziAlert("fa fa-info","Warning! ,","No bank selected.","bottomLeft","warning");
      }
    }
   
  }

  function edit_bank(bank_id){
    $("#modal_edit_bank").modal();

    var url = "../ajax/edit_bank.php";
    
    $.post(url,{id: bank_id}, function(data){
      if(data){
        $("#editBank_form").html(data);
      }
    });

  }

  function get_Allbanks(){
    $("#tbl_bank").DataTable().destroy();
    $('#tbl_bank').dataTable({
      "processing":true,
      "order": [[ 1, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/banks.php",
        "data":{
          test:"1"
        }
      },
      "order":[1,"asc"],
      "columnDefs":[{ targets:[0,2],orderable: false }],
      "columns":[
        {
          "mRender": function(data,type,row){
            return "<input type='checkbox' name='cb_bank' value='"+row.bank_id+"'>";
          }
        },
        {
          "data":"count"
        },
        {
          "mRender": function(data,type,row){
            
            return "<div class='dropdown'>"+
                      "<button type='button' class='text-center btn btn-sm btn-outline-dark' onclick='edit_bank("+row.bank_id+")'><i class='fa fa-edit mr-1'></i> Edit</button>"+
                    "</div>";
          }
        },
        {
          "data":"bank_name"
        }
      ]
    });
  }

</script>