<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Subsidiary</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Subsidiary</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">

            <div class="col-md-12 row">
              <div class="form-group col-4">
                  <span>Borrowers:</span>
                  <select id ="pensioneers" class="form-control select2" onchange="getLoan()">
                    <option value="0">&mdash; SELECT &mdash;</option>
                            <?php
                              $getPensioneers = mysqli_query($conn, "SELECT b.borrower_id,b.b_code,b.b_fname,b.b_lname from tbl_borrowers as b");
                               while($pensioneer_row = mysqli_fetch_array($getPensioneers)){
                            ?>
                              <option id="<?php echo $pensioneer_row['b_code'];?>" value="<?php echo $pensioneer_row['borrower_id'];?>"><?php echo ucfirst($pensioneer_row['b_fname'])." ".ucfirst($pensioneer_row['b_lname']);?></option>
                              
                            <?php } ?>
                  </select>
                </div>
                <div class="form-group col-3">
                    <span>Loan #:</span>
                    <select id="loans" class="form-control select2"></select>
                </div>

                <div class="col-2 pt-4">
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary" onclick="subsidiary_data();"><i class="fa fa-search"></i> Search</button>
                  </div>
                </div>
            </div>

            <div class="dropdown-divider col-md-12 mt-1 mb-3"></div>

            <table id="tbl_subsidiary" class="table table-sm table-bordered">
              <thead class="bg-dark">
              <tr>
                <th width="10px">#</th>
                <th>Loan #</th>
                <th>Due Date</th>
                <th>Principal</th>
                <th>Interest</th>
                <th>Total</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
              </tbody>                
            </table>
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </div><!--/. container-fluid -->
</section>
<!-- /.content -->
<?php require "template/modals/viewLoans_MD.php";?>
<script type="text/javascript">
  $(document).ready( function(){
    get_data(null,null);
  });

  function getLoan(){
    var pen_id = $("#pensioneers").val();
    var url = "../ajax/get_loans.php";
    $.post(url,{id: pen_id}, function(data){
      $("#loans").html(data).fadeIn();
    });
  }

  function subsidiary_data(){
    var pen_id = $("#pensioneers").val();
    var loan_id = $("#loans").val();

    if(loan_id && pen_id){
      get_data(pen_id,loan_id);
    }else{
      iziAlert("fa fa-info","Warning,","Please fill up required fields.","bottomLeft","warning");
    }
  }

 function get_data(pen_id,loan_id){

    $("#tbl_subsidiary").DataTable().destroy();
    $('#tbl_subsidiary').DataTable({
      "processing":true,
      "order": [[ 1, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/subsidiary.php",
        "dataSrc":"data",
        "data":{
          'pen_id':pen_id,
          'loan_id':loan_id,
        }
      },
      "columns":[
        {
          "data":"count"
        },
        {
          "data":"loan_no"
        },
        {
          "data":"date_added"
        },
        {
          "data":"principal"
        },
        {
          "data":"interest"
        },
        {
          "data":"EB"
        },
        {
          "data":"status"
        }
      ],"createdRow": function( row, data, dataIndex ) {
          if (data.status === "F" ) {
            $(row).css("background-color","#66bb6a");
          }
        }
    });
  }

</script>