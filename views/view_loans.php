<?php 
    $GBSettings = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_globals WHERE global_id = 1"));

    $get_max_loan_id = mysqli_num_rows(mysqli_query($conn, "SELECT * from tbl_loan"));
    $get_current_num = mysqli_fetch_array(mysqli_query($conn, "SELECT max(loan_no) as loan_num from tbl_loan"));
    $next_id = 0;
    if($get_max_loan_id == 0){
      $next_id = 1; 
    }else if($get_max_loan_id < $get_current_num[0]){
      $next_id = $get_current_num[0] - 1; 
    }else{
      $next_id = $get_max_loan_id + 1; 
    }

    if(isset($_GET["loan"]) && $_GET["loan"] != ""){
      $loan_id = $_GET["loan"];
      $view = "";
      $hide = "display: none;";
      $readOnly = "readonly";
      $disable = "disabled";

      $loan = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_loan WHERE loan_id = '$loan_id'"));
      $codeNum = $loan['code_no'];
      $pNote = $loan['p_note'];
      $B_id = $loan['pensioneers_id'];
      $loanNum = $loan['loan_no'];
      $type = $loan['loan_type'];
      $loanDate = date('Y-m-d', strtotime($loan['date']));
      $methodType = $loan['method_type'];
      if($methodType == "straight" || $methodType == ""){
        $cbStraight = "checked disabled";
        $cb76 = "disabled";
      }else{
        $cbStraight = "disabled";
        $cb76 = "checked disabled";
      }
      if($loan['bank_id'] == 0 && $loan['check_no'] == 0){
        $cbCash = "checked disabled";
        $cbCheck = "disabled";
      }else{
        $cbCash = "disabled";
        $cbCheck = "checked disabled";
      }
      $count_loans = mysqli_num_rows(mysqli_query($conn, "SELECT loan_id FROM tbl_loan WHERE pensioneers_id = '$B_id'"));
    }else{
      $view = "display: none;";
      $hide = "";
      $readOnly = "";
      $disable = "";
      $codeNum = "";
      $pNote = 0;
      $B_id = "";
      $loanNum = $next_id;
      $type = "";
      $loanDate = "";
      $methodType = "";
      $cbCash = "checked";
      $cbCheck = "";
    }
  ?>
  <style type="text/css">
    /*.tbl_deductions{
        white-space: normal;
    }
    tbody {
        display:block;
        height:150px;
        overflow:auto;
    }
    thead, tbody tr {
        display:table;
        width:100%;
        table-layout:fixed;
    }*/
  </style>
  <!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Loan Details</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('loans')?>">Loans</a></li>
          <li class="breadcrumb-item active">Loan Details</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="card">
      <div class="card-body">
        <div class="row">
          <?php if($loan["net_proceeds"] != "0.000"){ ?>
            <div class="col-md-4 offset-md-8">
              <div class="btn-group float-right">
                <button type="button" id="btn-cancel-loan" class="btn btn-danger" onclick="cancel_loans()"><i class="fa fa-ban"></i> Cancel Entry</button>  
              </div>
            </div>
         <?php } ?>
          <div class="col-md-12">
            <!-- <div class="card"> -->
              <!-- <div class="card-body"> -->
                <!-- <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active text-dark" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Main</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Loan Entry</a>
                  </li>
                </ul> -->
                <!--<div class="tab-content" id="custom-content-below-tabContent">
                  <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab"> -->
            <!-- H E A D E R -->
              <form id="form_add_loans" action="" method="POST">
                <div class="row">

                  <div class="col-md-2">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Code #:</b></span>
                      </div>
                      <input type="text" class="form-control" name="code_num" id="code_num" required="" value="<?php echo $codeNum;?>" <?php echo $readOnly; ?>>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Paid To:</b></span>
                      </div>
                      <select id="select_id" class="form-control select2" name="pensioneer" required="" <?php echo $disable; ?>>
                          <option value="0">&mdash; SELECT &mdash;</option>
                          <?php
                            $getBorrowers = mysqli_query($conn, "SELECT * from tbl_borrowers");
                             while($bRow = mysqli_fetch_array($getBorrowers)){
                          ?>
                            <option id="<?php echo $bRow['b_code'];?>" <?php if($B_id == $bRow['borrower_id']){echo "selected";}?> value="<?php echo $bRow['borrower_id'];?>"><?php echo ucfirst($bRow['b_fname'])." ".ucfirst($bRow['b_lname']);?></option>
                            
                          <?php } ?>
                        </select>
                    </div>
                  </div>

                  <div class="input-group mb-2 col-md-4"></div>

                  <div class="col-md-2">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Loan #:</b></span>
                      </div>
                      <input type="number" class="form-control" name="loan_num" value="<?php echo $loanNum;?>" readonly="" >
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Type:</b></span>
                      </div>
                      <select class="form-control select2" name="type" required="" <?php echo $disable; ?>>
                        <option value="">&mdash; SELECT &mdash;</option>
                        <option <?php if($type == 1){echo "selected";}?> value="1">Loan</option>
                     </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="input-group input-group-sm mb-2 ">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Date:</b></span>
                      </div>
                      <input type="date" class="form-control" name="date" required="" id="date" value="<?php echo $loanDate; ?>" <?php echo $readOnly; ?>>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group input-group-sm mb-2">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><b>Method:</b></span>
                        </div>
                        <div class="row">
                            <div class="mb-0 pl-3">
                            <label class="col-md-2">
                              <input type="checkbox" id="cb_76" onchange="methodCB()" name="cb_76" <?php echo $cb76; ?>>
                            </label>
                            <span class="col-md-8"> 76</span>
                          </div>

                          <div class="mb-0">
                            <label class="col-md-2">
                              <input type="checkbox" id="cb_straight" onchange="methodCB1()" name="cb_straight" checked="" <?php echo $cbStraight; ?>>
                            </label>
                            <span class="col-md-8">Straight</span>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group input-group-sm mb-2">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><b>Trans. Type:</b></span>
                        </div>

                        <div class="row">

                          <div class="mb-0 pl-3">
                            <label class="col-md-2">
                              <input type="checkbox" id="cb_cash" onchange="cbCash()" name="cb_cash" <?php echo $cbCash; ?>>
                            </label>
                            <span class="col-md-8"> Cash</span>
                          </div>

                          <div class="mb-0">
                            <label class="col-md-2">
                              <input type="checkbox" id="cb_check" onchange="cbCheck()" name="cb_check" <?php echo $cbCheck; ?>>
                            </label>
                            <span class="col-md-8">Check</span>
                          </div>

                        </div>

                    </div>
                  </div>

                  <div class="col-md-2 ">
                    <div class="input-group mb-2 check" style="display: none;">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Check #:</b></span>
                      </div>
                      <input type="text" class="form-control" name="check_num" value="<?php echo getCheckNum();?>">
                    </div>
                  </div>

                  <div class="input-group mb-2 col-md-4 check" style="display: none;">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><b>Bank:</b></span>
                    </div>
                    <select class="form-control select2" name="bank" required="">
                        <option value="0">&mdash; SELECT &mdash;</option>
                        <?php 
                          $getBank = mysqli_query($conn, "SELECT * FROM tbl_bank");
                          while($row = mysqli_fetch_array($getBank)){
                        ?>
                          <option value="<?php echo $row['bank_id'];?>"><?php echo $row['bank_name'];?></option>
                        <?php } ?>
                     </select>
                  </div>
                </div>

                <div class="col-md-12 mb-2 row" style="<?php echo $hide;?>">
                  <button type="submit" id="btn-add-loan" class="btn btn-secondary offset-md-10 btn-block"><i class="fa fa-check mr-2"></i>Add</button>
                </div>
              </form>
              <!-- /H E A D E R -->

              <!-- D E T A I L -->
              <form id="form_loan_details" method="POST" action="">
                <div class="row loan" style="<?php echo $view; ?>">
                  <div class="dropdown-divider mb-2 mt-1 col-12"></div>
                  <div class="col-md-4">
                    <div class="input-group input-group-sm mb-2 ">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Interest:</b></span>
                      </div>
                        <input type="number" class="form-control" name="interest" id="interest" required="" readonly="" value="<?php echo $loan['interest'];?>">
                        <input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
                    </div>
                  </div>

                  <div class="col-md-8"></div>

                  <div class="col-md-4">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Prom. Note:</b></span>
                      </div>
                        <input type="number" class="form-control" name="prom_note" id="prom_note" oninput="getGross()" required="" value="<?=$pNote;?>">
                        <input type="hidden" name="prom_note1" value="<?=$pNote;?>">
                    </div>
                  </div>

                  <div class="input-group input-group-sm mb-2 col-md-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><b>Term:</b></span>
                    </div>
                      <input type="number" class="form-control" name="term" id="term" oninput="getGross()" required="" value="<?php echo $loan['term']?>">
                  </div>

                  <div class="input-group input-group-sm mb-2 col-md-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><b>Rate:</b></span>
                    </div>
                      <input type="number" class="form-control" name="rate" id="rate" oninput="getGross()" value="<?php echo $GBSettings['global_value'];?>" required="">
                  </div>

                  <div class="input-group mb-2 col-md-4"></div>

                  <div class="col-md-4">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Change:</b></span>
                      </div>
                        <input type="number" class="form-control" name="change" id="change" value="0">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Cash Advance:</b></span>
                      </div>
                        <input type="number" class="form-control" name="cash_advance" id="cash_advance" value="0">
                    </div>
                  </div>

                  <div class="input-group mb-2 col-md-4"></div>
                  <div class="input-group mb-2 col-md-9"></div>

                  <div class="input-group mb-2 col-md-3">
                      <input type="number" class="form-control form-control-sm" name="" id="" value="0">
                  </div>

                  <div class="col-md-3 offset-md-6 text-right pt-2">
                    <span class="col-sm-6 text-sm"><b>Gross Cash Out:</b></span>
                  </div>

                  <div class="mb-2 col-md-3">
                      <input type="number" class="form-control form-control-sm" name="gross" id="gross" required="" readonly="" value="<?php echo $loan['gross_cash_out'];?>">
                  </div>
                </div>
                
                <div class="dropdown-divider col-md-12 mb-1"></div>

                <div class="row less_row" style="<?php echo $view; ?>">
                  
                <div class="col-md-12 m-0 p-0">
                  <label class="ml-1"><i>LESS:</i></label>
                </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-sm mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><b>Deductions:</b></span>
                      </div>
                        <select class="form-control form-control-sm select2" id="deduct_id">
                          <option value="0">Please Select:</option>
                          <?php
                          $getDeductions = mysqli_query($conn, "SELECT * FROM tbl_fees WHERE preset != 1");
                          while($deductions = mysqli_fetch_array($getDeductions)){
                            $val = $deductions["value"];
                            if($val == 0){
                              $deduc_fee = number_format(0,2);
                            }else{
                              $deduc_fee = number_format($val,2);
                            }
                          ?>
                            <option value="<?php echo $deductions['fees_id']; ?>"><?php echo $deductions['fees_name']; ?></option>
                          <?php } ?>
                        </select>
                    </div>
                  </div>

                  <div class="input-group mb-2 col-md-3">
                      <input type="number" class="form-control form-control-sm" id="deduction_amt" placeholder="Amount">
                  </div>

                  <div class="input-group mb-2 col-md-2">
                      <button type="button" class="btn btn-success" id="btn-add-deduct"><i class="fa fa-plus"></i></button>
                  </div>
                  
                  <div class="col-md-12 row insurance_row" >

                    <div class="mb-2 col-md-2">
                      <b>Insurance:</b>
                    </div>
                    <div class="mb-2 col-md-1">
                      <span class="text-left" id="d_ins">0.00</span>
                    </div>
                  </div>

                  <input type="hidden" name="d_ins_amt" id="d_ins_amt" value="0">
                  <input type="hidden" id="total_deduct">

                  <div class="col-8 p-0 tbl_wrap mb-2">
                    <table id="tbl_loan_deductions" class="table table-sm table-stripped">
                      <thead class="bg-secondary text-white">
                        <tr>
                          <th width="10px">#</th>
                          <th width="10px"></th>
                          <th>Deduction</th>
                          <th width="200px">Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                    <table class="table table-sm">
                      <tbody>
                        <tr style='background-color: #ccc;'>
                          <td colspan="3" class='font-weight-bold'>TOTAL DEDUCTIONS</td>
                          <td class='font-weight-bold total_deductions' width='235px'></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <div class="dropdown-divider col-md-12 mt-1 mb-1"></div>

                  <div class="col-md-3 offset-md-6 text-right text-sm pt-2">
                    <span><b>Others:</b></span>
                  </div>

                  <div class="input-group mb-2 col-md-3">
                    <input type="number" class="form-control form-control-sm" id="others" name="fees"  value="0.00" class="fees">
                  </div>

                    <div class="col-12">
                      <label class="float-right text-sm">
                        <input type="checkbox" id="old_acc" onclick="includeOldAcc()"> &nbsp; Include Old Account
                        <input type="hidden" id="add_old_acc" name="add_old_acc">
                      </label>
                    </div>

                    <div id="old_acc_wrapper" class="col-md-12 row" style="display: none;">
                      <label class="col-md-12"><i><u>TO CLOSE OLD ACCOUNT IF ANY:</u></i></label>

                      <div class="form-group row col-md-4">
                        <label for="inputPassword3" class="col-md-6 pl-5 control-label text-sm">Old Accounts:</label>
                        <div class="col-md-6">
                          <input type="number" class="form-control form-control-sm" name="old_accounts" id="old_accounts" value="0" min="0">
                        </div>
                      </div>

                      <div class="form-group row col-md-4">
                        <label for="inputPassword3" class="col-md-6 pl-5 control-label">UDI Rebate:</label>
                        <div class="col-md-6">
                          <input type="number" class="form-control form-control-sm" name="UBI_rebate" id="UBI_rebate" value="0" min="0">
                        </div>
                      </div>

                      <div class="form-group row col-md-4">
                        <label for="inputPassword3" class="col-md-6 pl-5 control-label">Coll. Fee Rebate:</label>
                        <div class="col-md-6">
                          <input type="number" class="form-control form-control-sm" name="coll_rebate" id="coll_rebate" value="0" min="0">
                        </div>
                      </div>

                    </div>

                    <div class="col-md-3 offset-md-9">
                      <input type="number" class="form-control form-control-sm" id="less_sum" readonly="">
                    </div>

                    <div class="col-md-3 offset-md-6 text-right">
                      <span for="exampleInputEmail1"><i class="text-primary text-sm">NET PROCEEDS:</i></span>
                    </div>

                    <div class="col-md-3 offset-md-9">
                      <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">-P-</span>
                        </div>
                        <input type="number" class="form-control" id="net_proceeds" name="net_proceeds" readonly="" value="<?php echo $loan['net_proceeds'];?>">
                      </div>
                    </div>

                    <div class="dropdown-divider col-md-12 mt-0 mb-1"></div>
                    <?php if($loan["net_proceeds"] == "0.000"){?>
                      <div class="col-md-12">
                        <div class="btn-group float-right mt-3">
                          <button type="submit" class="btn btn-secondary" id="btn-add"><i class="fa fa-save mr-2" ></i>Save Entry</button>
                        </div>
                      </div>
                	<?php } ?>

                  </div>

                </div>
              </form>
              <!-- /D E T A I L -->

                  <!-- </div>

                  <div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                     ENTRY
                  </div>
                </div> -->
                <!-- <div class="tab-custom-content">
                  <p class="lead mb-0">Custom Content goes here</p>
                </div> -->
               <!-- </div> -->
              <!-- /.card -->
            <!-- </div> -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </div>
    </div>

  </div>
</section>
<!-- <div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Loan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div> -->
      <!-- <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    <!-- </div> -->
    <!-- /.modal-content -->
  <!-- </div> -->
  <!-- /.modal-dialog -->
<!-- </div> -->
<!-- /.modal -->

<script type="text/javascript">

$(document).ready( function(){
  loanDeductions();
  getOthers();

  $("#select_id").change(function() {
    var id = $(this).children(":selected").attr("id");
    $("#code_num").val(id);
  });
  <?php if($loan["net_proceeds"] != 0){ ?>
    $("input").prop("readonly", true);
    $("select").prop("disabled", true);
    $(".btn").prop("disabled", true);
    $("input[type=checkbox]").prop("disabled", true);
    if(<?php echo $loan["status"]?> != 2){
    	$("#btn-cancel-loan").prop("disabled", false);
    }
    getGross();
  <?php } ?>
});

function loanDeductions(){

  $("#tbl_loan_deductions").DataTable().destroy();
  $("#tbl_loan_deductions").DataTable({
    "scrollY":        "150px",
    "scrollCollapse": true,
    "paging":         false,
    "bInfo":          false,
    'bSort':          false,
    "processing":true,
      "order": [[ 1, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/loan_deduct.php",
        "data":{
          loan_id: "<?php echo $loan_id;?>"
        }
      },
      "columns":[
        {
          "data":"count"
        },
        {
          "mRender": function(data,type,row){

            if(row.preset == 1){
              var disable = "disabled";
            }else{
             <?php if($loan["net_proceeds"] != 0.000){ echo "var disable = 'disabled';"; }else{ echo "var disable = '';"; }?>
            }
            $("#total_deduct").val(row.total_deduct);
            getAllLess();
            return "<button class='btn btn-sm btn-danger' "+disable+" onclick='delete_fee("+row.d_id+")'><i class='fa fa-trash'></i></button>"+
                    "<input type='hidden' name='fees' value='"+row.d_amount+"'>";
          }
        },
        {
          "data":"d_name"
        },
        {
          "data":"d_amount"
        }
      ]
  });

}

function cancel_loans(){
	var loan_id = "<?php echo $loan_id;?>";

	var x = confirm("Are you sure to cancel loan entry?");
  	var url = "../ajax/cancel_loans.php";

	if(x){
		 $("#btn-cancel-loan").prop("disabled", true);
	    $.post(url,{l_id: loan_id}, function(data){
	      if(data == 1){
	        iziAlert("fa fa-check","Success!","Loan entry was canceled","bottomLeft","success");
	        // $("#btn-cancel-loan").prop("disabled", false);
	       	setTimeout( function(){
	       		window.location.reload();
	       	},1500);
	      }else{
	        iziAlert("fa fa-times","Error!","Something was wrong.","bottomLeft","error");
	        alert(data);
	        $("#btn-cancel-loan").prop("disabled", false);
	      }
	    });
	}
}

$("#btn-add-deduct").click( function(){
  var deduction = $("#deduct_id").val();
  var deduct_amt = $("#deduction_amt").val();
  var loan_id = "<?php echo $loan_id;?>";
  var url = "../ajax/add_loan_deductions.php";

  if(deduction != "" && deduct_amt != ""){
    $("#btn-add-deduct").prop("disabled", true);
    $.post(url,{d_id: deduction,d_amt: deduct_amt, l_id: loan_id}, function(data){
      if(data == 1){
        iziAlert("fa fa-check","Success!","Loan deduction was added.","bottomLeft","success");
        $("#btn-add-deduct").prop("disabled", false);
        $("#deduct_id").val("");
        $("#deduction_amt").html("");
        loanDeductions();
      }else{
        iziAlert("fa fa-times","Error!","Something was wrong.","bottomLeft","error");
        alert(data);
        $("#btn-add-deduct").prop("disabled", false);
      }
    });
  }else{
    iziAlert("fa fa-info","Warning!","Please fill up deduction fields.","bottomLeft","warning");
  }
});

function delete_fee(id){
  var url = "../ajax/delete_loan_deductions.php";
  var x = confirm("Are you sure to delete deduction?");

  if(x){
    $(".btn-danger").prop("disabled", true);
    $.post(url,{d_id: id}, function(data){
      if(data == 1){
        iziAlert("fa fa-check","Success!","Loan deduction was deleted.","bottomLeft","success");
        $(".btn-danger").prop("disabled", false);
        loanDeductions();
      }else{
        iziAlert("fa fa-times","Error!","Something was wrong.","bottomLeft","error");
        alert(data);
        $(".btn-danger").prop("disabled", false);
      }
    });
  }
}

function cbCash(){
  var cb_cash = $("#cb_cash").is(":checked");

  if(cb_cash){
    $("#cb_check").prop("checked", false);
    $(".check").hide();
    $(".check_div").show();
  }else{
    $("#cb_cash").prop("checked", true);
    $("#cb_check").prop("checked", false);

    $(".check").hide();
    $(".check_div").show();
  }
}

function cbCheck(){
  var cb_check = $("#cb_check").is(":checked");

  if(cb_check){
    $("#cb_cash").prop("checked", false);
    $(".check").show();
    $(".check_div").hide();
  }else{
    $("#cb_cash").prop("checked", true);
    $("#cb_check").prop("checked", false);

    $(".check").hide();
    $(".check_div").show();
  }
}

$("#form_add_loans").submit( function(e){
    e.preventDefault();

    var x = $("#cb_cash").is(":checked");
    if(x){
      $("#cb_cash").val(1);
      $("#cb_check").val(0);
    }else{
      $("#cb_cash").val(0);
      $("#cb_check").val(1);
    }

    var sendData = $(this).serialize();
    $("#btn-add-loan").prop("disabled", true);
    $("#btn-add-loan").html("Saving <i class='fa fa-spin fa-redo-alt'></i>");
   $.ajax({
          url: "../ajax/add_loans.php",
          type: "POST",
          data: sendData,
          success: function (response) {
            if(response != "error"){
              iziAlert("fa fa-check","Success! ,","A new loan was added.","bottomLeft","success");
              setTimeout( function(){
                window.location="index.php?page=add-loan&loan="+response;
              },2100);
            }else{
              alert("failed");
              alert(response);
              $("#btn-add-loan").prop("disabled", false);
              $("#btn-add-loan").html("<i class='fa fa-check'></i> Save Entry");
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
             console.log(textStatus, errorThrown);
          }
   });
  });

$("#form_loan_details").submit( function(e){
    e.preventDefault();

    var x = $("#cb_cash").is(":checked");
    if(x){
      $("#cb_cash").val(1);
      $("#cb_check").val(0);
    }else{
      $("#cb_cash").val(0);
      $("#cb_check").val(1);
    }

    var sendData = $(this).serialize();
    $("#btn-add").prop("disabled", true);
    $("#btn-add").html("Saving <i class='fa fa-spin fa-redo-alt'></i>");
   $.ajax({
          url: "../ajax/add_loan_details_test.php",
          type: "POST",
          data: sendData,
          success: function (response) {
            if(response == 1){
              iziAlert("fa fa-check","Success! ,","A new loan entry was added.","bottomLeft","success");
              setTimeout( function(){
                window.location="index.php?page=loans";
              },2100);
            }else{
              alert("failed");
              $("#btn-add").prop("disabled", false);
              $("#btn-add").html("<i class='fa fa-check'></i> Save Entry");
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
             console.log(textStatus, errorThrown);
          }
   });
  });

function methodCB(){
  var cb_76 = $("#cb_76").is(":checked");
  var cb_straight = $("#cb_straight").is(":checked");

  if(cb_76){
    $("#cb_straight").prop("checked", false);
    document.getElementById("cb_76").value = '1';
  }else{
    $("#cb_76").prop("checked", false);
    $("#cb_straight").prop("checked", true);

  }
}

function methodCB1(){
  var cb_76 = $("#cb_76").is(":checked");
  var cb_straight = $("#cb_straight").is(":checked");

  if(cb_straight){
    $("#cb_76").prop("checked", false);
     document.getElementById("cb_76").value = '0';
  }else{
    $("#cb_76").prop("checked", false);
    $("#cb_straight").prop("checked", true);

  }
}

      function getGross(){
        var prom_note = $("#prom_note").val();
        var rate = $("#rate").val();
        var term = $("#term").val();

        var x = ((rate * prom_note)/100)*term;
        var y = prom_note-x;

        if(prom_note == 0){
          var pNote = $("input[name=prom_note1]").val();
          getInsurance(pNote);
        }else{
          var pNote = $("#prom_note").val();
          getInsurance(pNote);
        }

        if(prom_note && rate && term){

          $("#interest").val(parseFloat(x.toFixed(2)));
          $("#gross").val(y.toFixed(2));

          getAllLess();

        }
      }

      function getInsurance(prom_note){
      	if(prom_note <= 1000){
      		insurance = 200;
      		$("#d_ins").html(insurance);
      		$("#d_ins_amt").val(insurance);
      	}else if(prom_note <= 2000){
			    insurance = 300;
      		$("#d_ins").html(insurance);
      		$("#d_ins_amt").val(insurance);
      	}else if(prom_note <= 3000){
			    insurance = 400;
      		$("#d_ins").html(insurance);
      		$("#d_ins_amt").val(insurance);
      	}else{
      		insurance = 500;
      		$("#d_ins").html(insurance);
      		$("#d_ins_amt").val(insurance);
      	}

  //     	var arr = [];
		// var sum = 0;

		// $('input[name=fees]').each(function () {
		//       arr.push($(this).val());
		//   });

		// $.each(arr,function(){sum+=parseFloat(this) || 0;});
		// $(".total_deductions").html(sum.toFixed(2));

      }

      function getOthers(){
        var a = $("#old_accounts").val();
        var b = $("#UBI_rebate").val();
        var c = $("#coll_rebate").val();

        var y = parseInt(b) + parseInt(c);
        var x = a - y;

        $("#others").val(x.toFixed(2));
      }

      function getAllLess(){

        var a = $("#others").val();
        var b = $("#gross").val();

        // var arr = [];
        // var sum = 0;

        // $('input[name=fees]').each(function () {
        //     arr.push($(this).val());
        // });

        // $.each(arr,function(){sum+=parseFloat(this) || 0;});
        // $(".total_deductions").html(sum.toFixed(2));

        // var y = parseFloat(a) + parseFloat(sum);
        // var prom_note = $("input[name=prom_note1]").val();
        // if(prom_note != 0){
          var sum = parseFloat($("#total_deduct").val());
          var ins = parseFloat($("#d_ins_amt").val());
          var deduct = sum + ins;
        // }
        // else{
        //   var sum = parseFloat($("#total_deduct").val());
        //   var deduct = sum;
        // }
        var y = parseFloat(a) + deduct;

        var x = b - y;

       $("#less_sum").val(y.toFixed(2));
       $("#net_proceeds").val(x.toFixed(2));
       $(".total_deductions").html(deduct.toFixed(2));

      }

      function includeOldAcc(){
        var x = $("#old_acc").is(":checked");

        if(x){
          $("#old_acc_wrapper").show();
        }else{
          $("#old_acc_wrapper").hide();
        }
      }

</script>