<?php
	$loan_id = $_GET['id'];

	$pen_id = get_pensioneer_id($loan_id);
	$pensioneer_name = get_pensioneer_name($pen_id, $conn);
	$get_max_subsidiary_id = mysqli_fetch_array(mysqli_query($conn, "SELECT max(subsidiary_id) as max_sub,interest FROM `tbl_subsidiary` where loan_id ='$loan_id' and pensioneer_id ='$pen_id' "));
	$max_subsidiary = $get_max_subsidiary_id['max_sub'];
	$interest = $get_max_subsidiary_id['interest'];
	$row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_loan WHERE loan_id = '$loan_id'"));
	$pcode = get_pensioneer_code($row["pensioneers_id"]);

	$loan_no = $row['loan_no'];
	$due_date = date("Y-m-d",strtotime($row["date"]));
	$now = date("Y-m-d");
?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Add Official Receipt</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
          <li class="breadcrumb-item"><a href="index.php?page=official-receipt">Official Receipt</a></li>
          <li class="breadcrumb-item active">Add Official Receipt</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
     	<div class="col-md-12">
	        <div class="card">
		        <div class="card-body">
		        	<div class="row">
		        		<div class="col-md-12 row p-3">
		        			<div class="col-md-6 mb-2">
							    <h4>Loan No. <?php echo $loan_no;?> - <?php echo $pensioneer_name;?></h4> 
							 </div>
		        			 <div class="col-md-6">
					              <div class="float-right">
					                <button type="button" class="btn btn-success mr-1" data-toggle="modal" data-target="#modal_add_deduction" onclick="issue_OR('Cash')"><i class="fa fa-money-bill-wave"></i> Issue Cash </button>

					                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_deduction" onclick="issue_OR('Check')"><i class="fa fa-money-check-alt"></i> Issue Check </button>
					              </div>
							 </div>
	        				<!-- <div class="form-group col-md-4">
	        			
	                         	<label class="col-12">OR #:</label>
	                         	<div class="input-group mb-3"> -->
		                          	<!-- <select class="form-control col-2">
		                          		<option>OR</option>
		                          	</select> -->
		                <!--           	<input type="text" class="form-control col-12" name="or_num" id="or_num" value="<?php echo getORNum(); ?>">
	                          	</div>
	                        </div> -->

	                      <!--   <div class="form-group col-md-4">
	                        	<label for="exampleInputEmail1">Name:</label>
		                        <div class="input-group mb-3">
					                <input type="text" class="form-control" readonly="" id="pen_name" name="pen_name" value="<?php echo $pensioneer_name;?>">
					                <div class="input-group-append">
										<span class="input-group-text text-danger"><?php echo $pcode;?></span>
					                </div>
					            </div>
	                        </div> -->

	                  <!--       <div class="form-group col-md-4">
	                          <label for="exampleInputEmail1">Date:</label>
	                          <input type="date" class="form-control" name="date_added" id="date_added" required="" value="<?php echo $now;?>">
	                        </div>

	                        <div class="form-group col-md-4">
	                          <label for="exampleInputEmail1">Type:</label>
	                          <select class="form-control" id="type">
	                          	<option value="1">Loan</option>
	                          </select>
	                        </div>

	                        <div class="form-group col-md-4">
	                          <label for="exampleInputEmail1">Amount:</label>
	                          <input type="text" class="form-control" name="pay_amt" id="pay_amt" required="">
	                        </div>

	                        <div class="form-group col-md-6">
	                          <label class="row offset-md-4">
	                          	<input type="checkbox" class="col-sm-1 mt-1" id="pay_full"> 
	                          	<i class="col-sm-10 text-left">FULL PAYMENT</i>
	                          </label>
	                        </div>

	                        <div class="form-group col-md-6">
	                        	<div class="row offset-md-4">
	                        		<label class="col-md-4">
	                        			<div class="row">
		                        			<input type="checkbox" class="col-sm-2 mt-1" id="pay_cash" checked="">
		                        			<span class="col-sm-10">CASH</span>
										</div>
									</label>
	                          		<label class="col-md-4">
	                          			<div class="row">
		                        			<input type="checkbox" class="col-sm-2 mt-1" id="pay_charge">
		                        			<span class="col-sm-10">CHARGE</span>
										</div>
	                          		</label>
	                        	</div>
	                        </div>

	                        <div class="col-md-12 row" id="check_or" style="display: none;">
	                        	<div class="form-group col-md-4">
		                          <label for="exampleInputEmail1">Pen. Bank:</label>
		                          <input type="text" class="form-control" name="pen_bank" id="pen_bank" required="">
		                        </div>

		                        <div class="form-group col-md-3">
		                          <label for="exampleInputEmail1">Bank:</label>
		                          <select class="form-control" id="bank">
		                          	<option></option>
		                          </select>
		                        </div>

		                        <div class="form-group col-md-2">
		                          <label for="exampleInputEmail1">Check #:</label>
		                          <input type="number" class="form-control" name="check_num" id="check_num" required="">
		                        </div>

		                        <div class="form-group col-md-3">
		                          <label for="exampleInputEmail1">Due to SSP:</label>
		                          <input type="number" class="form-control" name="due_to_ssp" id="due_to_ssp" required="">
		                        </div>
	                        </div> -->

	                        <div class="dropdown-divider col-md-12 mt-0 mb-1"></div>

	                        <div class="col-md-12 mt-2 p-3" style="border: 1px solid #ccc; border-radius: 6px;">
	                        	
	                        	<ul class="nav nav-tabs col-md-12 row" id="custom-content-above-tab" role="tablist">
					              <li class="nav-item col-md-6">
					                <a class="nav-link active text-center text-dark" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Ammortization</a>
					              </li>
					              <li class="nav-item col-md-6">
					                <a class="nav-link text-center text-dark" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">OR Entry</a>
					              </li>
					            </ul>

					            <div class="col-md-12 mt-2 mb-1">
					            	
					            	<div class="tab-content" id="custom-content-above-tabContent">
						              <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
						              	<div class="row">
						              	
							            <div class="col-md-12 mt-2">
						                  <table id="tbl_subsidiary" class="table table-sm table-bordered table-striped">
							              <thead>
							              <tr class="bg-dark">
							              	<th width="5px"><input type="checkbox" id="checkAll" onchange="checkAll()"></th>
							                <th width="5px">#</th>
							                <th width="10px"></th>
							                <th>Reference #</th>
							                <th>Due Date</th>
							                <th>Penalty</th>
							                <th>Principal</th>
							                <th>Interest</th>
							                <th>Total</th>
							                <th>Status</th>
							              </tr>
							              </thead>
							              <tbody>
							              </tbody>                
							            </table>
							        </div>
							        </div>
						              </div>
						              <div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
						                <table id="tbl_official_receipt" class="table table-sm table-bordered table-striped text-center" width="100%">
								            <thead>
									            <tr class="bg-dark">
									                <th width="10px">#</th>
									                <th width="10px"></th>
									                <th>Date issued</th>									 
									                <th>O.R. #</th>
									                <th>Amount</th>
									                <th>Payment Type</th>	
									            </tr>
								            </thead>
								            <tbody>
								            </tbody>                
								        </table>
						              </div>
						            </div>

					            </div>

	                        </div>

		        		</div>
		        	</div>
		        </div>
	      	</div>
    	</div>
  	</div>

  </div><!--/. container-fluid -->
</section> <!-- /.content -->
<?php include "template/modals/add_penalty.php"; ?>
<?php include "template/modals/issue_OR.php"; ?>
<?php include "template/modals/add_partial.php"; ?>
<script type="text/javascript">
 var current_or_num = "<?php echo getORNum();?>";
  function checkAll(){
    var cb = $("#checkAll").is(":checked");
    if(cb){
      $("input[name='cb_subsidiary']").prop("checked", true);
    }else{
      $("input[name='cb_subsidiary']").prop("checked", false);
    }
  }

	$(document).ready( function(){
		var loan_id = "<?php echo $loan_id;?>";
		var pen_id = "<?php echo $pen_id;?>";
		get_data(pen_id,loan_id);
		get_data_official_receipt();
	
	});

	function printReceipt(id){
		window.location="index.php?page=print-official-receipt&id="+id;
		// alert("print!");
	}

	//modals
function add_penalty(remaining_balance,subsidiary_id){
	var loan_id = "<?php echo $loan_id;?>";
    var url = "../ajax/add_penalty_modal.php";
    $.post(url,{loan_id:loan_id,subsidiary_id:subsidiary_id,remaining_balance:remaining_balance}, function(data){
      $("#modal_add_penalty").modal();
      $(".edit_body").html(data);
    });
  }

  function add_partial(remaining_balance,subsidiary_id){
	var loan_id = "<?php echo $loan_id;?>";
    var url = "../ajax/add_partial_modal.php";
    $.post(url,{loan_id:loan_id,subsidiary_id:subsidiary_id,remaining_balance:remaining_balance}, function(data){
      $("#modal_add_partial").modal();
      $(".edit_body_partial").html(data);
    });
  }


 function issue_OR(payment_type){
 	get_or_num();
 	var max_sub = <?php echo $max_subsidiary;?>;
 	var interest = <?php echo $interest;?>;
 	 var or_num = current_or_num;
  	 var checkedVals = [];
 	 $('input[name=cb_subsidiary]:checked').map(function() {
      checkedVals.push($(this).val());
    });

 	 if(checkedVals.length==0){
 	 	alert("Please select ammortization schedule.");
 	 }else{
	 	 var url = "../ajax/issue_OR_modal.php";
	    $.post(url,{max_sub:max_sub,checkedVals:checkedVals,or_num:or_num,payment_type:payment_type,interest:interest}, function(data){
	      $("#modal_issue_OR").modal();
	      $(".edit_body_issue_OR").html(data);
	    });
 	 }


  }

	$("#pay_cash").click( function(){
		var a = $("#pay_cash").is(":checked");
		if(a){
			$("#pay_cash").prop("checked", true);
			$("#pay_charge").prop("checked", false);
			$("#check_or").hide();
		}else{
			$("#pay_cash").prop("checked", false);
			$("#pay_charge").prop("checked", true);
			$("#check_or").show();
		}
	});

	$("#pay_charge").click( function(){
		var a = $("#pay_charge").is(":checked");
		if(a){
			$("#pay_cash").prop("checked", false);
			$("#pay_charge").prop("checked", true);
			$("#check_or").show();
		}else{
			$("#pay_cash").prop("checked", true);
			$("#pay_charge").prop("checked", false);
			$("#check_or").hide();
		}
	});

	function get_or_num(){
	var url = "../ajax/get_or_num.php";
    $.post(url,{},function(data){
     	current_or_num= data;
    });
	}

	function issue_final_Receipt(amount,change,issue_date,partial_check,payment_type,bank_id,check_num){
		get_or_num();	
		var or_num = current_or_num;
		var loan_id = "<?php echo $loan_id;?>";

		var subsidiary_ids = [];
		$('input[name=cb_subsidiary]:checked').map(function() {
		      subsidiary_ids.push($(this).val());
		});


		var x = confirm("Are you sure to Issue Official Receipt?");
		var url = "../ajax/issue_receipt_full_payment.php";
		if(x){
			if(or_num != "" || or_num != 0){
				$.post(url,
				{
					partial_check:partial_check,loan_id:loan_id,subsidiary_ids:subsidiary_ids,amount:amount,change:change,or_num:or_num,issue_date:issue_date,payment_type:payment_type,bank_id:bank_id,check_num:check_num
					
				},
				function(data){
					//alert(data);

					if(data == 1){
						get_or_num();
						var loan_id = "<?php echo $loan_id;?>";
						var pen_id = "<?php echo $pen_id;?>";
						iziAlert("fa fa-check","Success!,","Official receipt was successfully issued.","bottomLeft","success");
						get_data(pen_id,loan_id);
						get_data_official_receipt();
						// var loan_id = "<?php echo $loan_id;?>";
						// var pen_id = "<?php echo $pen_id;?>";
						// setTimeout( function(){
						// 	var print = confirm("Do you want to print Official Receipt?");
						// 	if(print){
						// 		window.location="index.php?page=print-official-receipt&id="+row[0];
						// 	}else{
						// 		window.location="index.php?page=add-official-receipt&id="+loan_id;
						// 		get_data(pen_id,loan_id);
						// 		get_data_official_receipt(pen_id,loan_id);
						// 	}
						// },1500);
					
					}else{
						iziAlert("fa fa-times","Error!,","Something was wrong.","bottomLeft","error");
						
					}
				});
			}else{
				iziAlert("fa fa-info","Warning","Please fill up required fields.","bottomLeft","warning");
			}
		}
	}

	function issue_final_Receipt_partial(payment,amount,change,issue_date,partial_check,payment_type,bank_id,check_num){
		get_or_num();	
		var or_num = current_or_num;
		var loan_id = "<?php echo $loan_id;?>";

		var subsidiary_ids = [];
		$('input[name=cb_subsidiary]:checked').map(function() {
		      subsidiary_ids.push($(this).val());
		});
		var x = confirm("Are you sure to contintue as partial payment?");
		var url = "../ajax/issue_receipt_partial_payment.php";

		if(subsidiary_ids.length==1){
			if(x){
			if(or_num != "" || or_num != 0){
				$.post(url,
				{
					partial_check:partial_check,loan_id:loan_id,subsidiary_ids:subsidiary_ids,amount:payment,change:change,or_num:or_num,issue_date:issue_date,payment_type:payment_type,bank_id:bank_id,check_num:check_num
					
				},
				function(data){
					//alert(data);

					if(data == 1){
						get_or_num();
						var loan_id = "<?php echo $loan_id;?>";
						var pen_id = "<?php echo $pen_id;?>";
						iziAlert("fa fa-check","Success!,","Official receipt was successfully issued.","bottomLeft","success");
						get_data(pen_id,loan_id);
						get_data_official_receipt();
						// var loan_id = "<?php echo $loan_id;?>";
						// var pen_id = "<?php echo $pen_id;?>";
						// setTimeout( function(){
						// 	var print = confirm("Do you want to print Official Receipt?");
						// 	if(print){
						// 		window.location="index.php?page=print-official-receipt&id="+row[0];
						// 	}else{
						// 		window.location="index.php?page=add-official-receipt&id="+loan_id;
						// 		get_data(pen_id,loan_id);
						// 		get_data_official_receipt(pen_id,loan_id);
						// 	}
						// },1500);

					}else{
						iziAlert("fa fa-times","Error!,","Something was wrong.","bottomLeft","error");
						
					}
				});
			}else{
				iziAlert("fa fa-info","Warning","Please fill up required fields.","bottomLeft","warning");
			}
		}

		}else{
			alert("System error. Please select only one ammortization schedule for partial payment.");
		}
		
	}


// function add_partial_modal(subsidiary_id,remaining_balance,amount){
//             $.ajax({
//           url: "../ajax/add_partial.php",
//           type: "post",
//           data: {subsidiary_id:subsidiary_id,amount:amount},
//           success: function (response) {
//            //   $("#btn_add_partial").prop("disabled", false);
//             if(response==1){
//                iziAlert("fa fa-check","Success! ,","Partial was added.","bottomLeft","success");
//         s
//             }else{
//               iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
//             }
//           },
//           error: function(jqXHR, textStatus, errorThrown) {
//            //   $("#btn_add_partial").prop("disabled", false);
//              console.log(textStatus, errorThrown);
//           }
//         });

      
   
//  }


	function issueReceipt(id){
		var or_num = <?php echo getORNum();?>;
		var date_added = $("#date_added").val();
		var type = $("#type").val();
		var pen_bank = $("#pen_bank").val();
		var check_num = $("#check_num").val();
		var bank = $("#bank").val();
		var due_to_ssp = $("#due_to_ssp").val();

		if($("#pay_full").is(":checked")){
			var pay_full = 1;
		}else{
			var pay_full = 0;
		}

		if($("#pay_cash").is(":checked")){
			var pay_type = 0;
		}else{
			var pay_type = 1;
		}
		var loan_id = "<?php echo $loan_id;?>";

		var x = confirm("Are you sure to Issue Official Receipt?");
		var url = "../ajax/issue_receipt.php";
		if(x){
			if(date_added && pay_type && or_num != "" || or_num != 0){
				$.post(url,
				{
					id:id,
					or_num:or_num,
					date_added:date_added,
					type:type,
					pen_bank:pen_bank,
					check_num: check_num,
					bank:bank,
					due_to_ssp:due_to_ssp,
					pay_type:pay_type,
					pay_full:pay_full,
					loan_id: loan_id
				},
				function(data){
					var row = data.split("-");

					if(row[1] == 1){
						iziAlert("fa fa-check","Success!,","Official receipt was successfully issued.","bottomLeft","success");
						var loan_id = "<?php echo $loan_id;?>";
						var pen_id = "<?php echo $pen_id;?>";
						setTimeout( function(){
							var print = confirm("Do you want to print Official Receipt?");
							if(print){
								window.location="index.php?page=print-official-receipt&id="+row[0];
							}else{
								window.location="index.php?page=add-official-receipt&id="+loan_id;
								get_data(pen_id,loan_id);
								get_data_official_receipt();
							}
						},1500);
					}else if(row[1] == 2){
						iziAlert("fa fa-info","Warning!,","Unable to issue receipt, entry not yet due.","bottomLeft","warning");
					}else{
						iziAlert("fa fa-times","Error!,","Something was wrong.","bottomLeft","error");
						alert(data);
					}
				});
			}else{
				iziAlert("fa fa-info","Warning","Please fill up required fields.","bottomLeft","warning");
			}
		}
	}

	function get_data_official_receipt(){
				var loan_id = "<?php echo $loan_id;?>";
				var pen_id = "<?php echo $pen_id;?>";
	    $("#tbl_subsidiary").DataTable().destroy();
	    $('#tbl_subsidiary').DataTable({
	      "processing":true,
	      "ajax":{
	        "type":"POST",
	        "url":"../ajax/datatables/official_receipt.php",
	        "dataSrc":"data",
	        "data":{
	          'pen_id':pen_id,
	          'loan_id':loan_id,
	        }
	      },
	      "columnDefs": [
	        { targets: [0,2], orderable: false}
	    	],
	      "order": [1,"asc"],
	      "columns":[
	        {
	          "mRender": function(data,type,row){
	          	if(row)
	            return "<input type='checkbox' name='cb_subsidiary' value='"+row.id+"' "+row.disable+">";
	          }
    	    },
	        {
	          "data":"count"
	        },
	        {
		      	"mRender": function(data,type,row){

		      		$("#pay_amt").val(row.EB);

		      		if(row.status != "F"){
	            		return "<div class='dropdown'>"+
		                      "<a href='#' data-toggle='dropdown'><i class='fa fa-cog text-dark ml-1'></i></a>"+
		                        "<div class='dropdown-menu'>"+
		                        "<a class='dropdown-item text-sm' href='#' onclick='add_penalty("+row.remaining_balance+","+row.id+")'> View/Edit Penalty</a>"+
		                        "<a class='dropdown-item text-sm' href='#' onclick='add_partial("+row.remaining_balance_partial+","+row.id+")'> View Partial</a>"+
		                       
		                    "</div>";
	            	}else{ 
	            		return "<i class='fa fa-ban text-dark ml-1'></i>";
	            	} 
		      	}
	        },
	        {
	          "data":"subsidiary_id"
	        },
	        {
	          "data":"date_added"
	        }, {
	          "data":"penalty"
	        },
	        {
	          "data":"principal"
	        },
	        {
	          "data":"interest"
	        },
	        {
	          "data":"EB"
	        },
	        {
	          "mRender": function(data,type,row){
	          	if(row.status == "F"){
	          		return "<span class='text-white text-bold'>Paid</span>";
	          	}else{
	          		if(row.overdue == 1){
		          		return "<span class='text-warning text-bold'>Over Due</span>";
		          	}else{
		          		if(row.partial==1){
		          			return "<span class='text-danger text-bold'>Partial</span>";
		          		}else{
		          			return "<span class='text-primary text-bold'>Pending</span>";
		          		}

	          			
		          	}
	          	}
	          }
	        }
	      ],"createdRow": function( row, data, dataIndex ) {
	          if (data.status === "F" ) {
	            $(row).css("background-color","#66bb6a");
	          }
	        }
	    });
	}

	function get_data(pen_id,loan_id){

	    $("#tbl_official_receipt").DataTable().destroy();
	    $('#tbl_official_receipt').DataTable({
	      "processing":true,
	      "ajax":{
	        "type":"POST",
	        "url":"../ajax/datatables/official_receipt_entry.php",
	        "dataSrc":"data",
	        "data":{
	          'pen_id':pen_id,
	          'loan_id':loan_id,
	          'official_receipt':1,
	        }
	      },
	      "columnDefs": [
	        { targets: [1], orderable: false}
	      ],
	      "columns":[
	        {
	          "data":"count"
	        },
	        {
		      	"mRender": function(data,type,row){

		      		$("#pay_amt").val(row.EB);

		      		if(row.status == "P"){
		      			var hide = "style='display:none;'";
		      			var hide1 = "";
		      		}else{
		      			var hide = "";
		      			var hide1 = "style='display:none;'";
		      		}
		      		// if(row.issuable == 1){
		            	return "<div class='dropdown'>"+
		                      "<a href='#' data-toggle='dropdown'><i class='fa fa-cog text-dark ml-1'></i></a>"+
		                        "<div class='dropdown-menu'>"+
		                        "<a class='dropdown-item text-sm' href='#' "+hide1+" onclick='issueReceipt("+row.id+")'><i class='fa fa-file text-dark mr-1'></i> Issue Official Receipt</a>"+
		                        "<div class='divider'></div>"+
		                        "<a class='dropdown-item text-sm' "+hide+" href='#' onclick='printReceipt("+row.id+")'><i class='fa fa-print text-dark mr-1'></i> Print</a>"+
		                      "</div>"+
		                    "</div>";
		            	// }else{ 
		            	// 	return "<i class='fa fa-ban text-dark ml-1'></i>";
		            	// } 
		      	}
	        },
	        {
	          "data":"date_issued"
	        },
	        {
	          "data":"or_num"
	        },
	        {
	          "data":"amount"
	        },
	        {
	          "data":"payment_type"
	        }
	        // ,
	        // {
	        //   "mRender": function(data,type,row){
	        //   	if(row.status == "F"){
	        //   		return "<span class='text-white text-bold'>Paid</span>";
	        //   	}else{
	        //   		return "<span class='text-primary text-bold'>Pending</span>";
	        //   	}
	        //   }
	        // }
	      ]
	      // ,"createdRow": function( row, data, dataIndex ) {
	      //     if (data.status === "F" ) {
	      //       $(row).css("background-color","#66bb6a");
	      //     }
	      //   }
	    });
	}
</script>