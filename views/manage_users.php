<?php
  $now = date("Y-m-d");

?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Manage Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
              <li class="breadcrumb-item active">Manage Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <div class="row mb-2">
        <div class="col-12">
          <div class="float-sm-right">
            <button class="btn btn-secondary" data-toggle="modal" data-target="#modal_register"><i class="fa fa-user-plus"></i> Add</button>
            <button class="btn btn-warning" onclick="changeStatus()"><i class="fa fa-sync"></i> Change Status</button>
            <button class="btn btn-danger" onclick="deleteUser()"><i class="fa fa-trash"></i> Delete</button>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="fa fa-users mr-2"></i> Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tbl_users" class="table table-bordered table-hover">
                <thead>
                <tr class="bg-dark">
                  <th width="15px"><input type="checkbox" id="checkAll" onclick="checkAll()"></th>
                  <th width="15px">#</th>
                  <th>Name</th>
                  <th width="150px;" class="text-center" >Role</th>
                  <th width="100px;" class="text-center" >Status</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
<?php include "template/modals/register_MD.php"; ?>

<script type="text/javascript">
  $(document).ready( function(){
    getUsers();
  });

  function checkAll(){
    var x = $("#checkAll").is(":checked");
    if(x){
      $("input[name=users]").prop("checked", true);
    }else{
      $("input[name=users]").prop("checked", false);
    }
  }

  function getUsers(){
    $("#tbl_users").DataTable().destroy();
    $("#tbl_users").dataTable({
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/users.php",
        "data":"",
        "processing":true
      },
      "columns":[
      {
        "mRender": function(data,type,row){
          return "<input type='checkbox' name='users' value='"+row.u_id+"'>";
        }
      },
      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"role"
      },
      {
        "data":"status"
      }
      ]
    });
  }

  function deleteUser(){
     var allVals = [];
      $("input[name=users]:checked").each( function(){
        allVals.push($(this).val());
      });
      if(allVals != ""){
        var x = confirm("Are you sure to delete user?");
        if(x){
          var users = allVals.toString();
          $("#modal_confirm").modal();
          $("#user_id").val(users);
        }
      }else{
        iziAlert("fa fa-info","Warning","No Data Selected.","bottomLeft","warning");
      }
  }

  function changeStatus(){

     var allVals = [];
    $("input[name=users]:checked").each( function(){
      allVals.push($(this).val());
    });
    if(allVals != ""){
      var x = confirm("Are you sure to change user status?");

      if(x){
        var url = "../ajax/updateStat.php";
        $.post(url,{id: allVals}, function(data){
          if(data == 1){
            iziAlert("fa fa-check","Success!, ","User status was successfully updated.","bottomLeft","success");
            getUsers();
          }else{
            iziAlert("fa fa-ban","Error!, ","Something was wrong.","bottomLeft","error");
          }
        });
      }
    }else{
      iziAlert("fa fa-info","Warning","No Data Selected.","bottomLeft","warning");
    }
  }

</script>