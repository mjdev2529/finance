<?php 

  include '../../core/conn.php';

  $id = $_GET["id"];

  $now = date("Y-m-d");

  $get_or_details = mysqli_fetch_array(mysqli_query($conn, "SELECT * from tbl_official_receipt where or_id = '$id'"));

  if($get_or_details['partial_payment_id']>0){
    $sql = mysqli_query($conn, "SELECT a.subsidiary_id,sum(b.penalty_amount) as penalty,a.principal,a.interest,a.pensioneer_id,a.loan_num from `tbl_subsidiary` a left join tbl_payment_penalty b on a.subsidiary_id=b.subsidiary_id where a.issued_or_id = '$id' group by a.subsidiary_id")or die(mysqli_error());

  }else{
     $sql = mysqli_query($conn, "SELECT a.subsidiary_id,sum(b.penalty_amount) as penalty,a.principal,a.interest,a.pensioneer_id,a.loan_num, sum(c.amount) partial from `tbl_subsidiary` a left join tbl_payment_penalty b on a.subsidiary_id=b.subsidiary_id left join tbl_partial_payment c on a.subsidiary_id = c.subsidiary_id where a.issued_or_id = '$id' group by a.subsidiary_id")or die(mysqli_error());

  }

 

  while($row = mysqli_fetch_array($sql)){
      $amount_payment  += $row["interest"]+$row["principal"]+$row["penalty"]-$row["partial"];
      $pensioneer_id = getPensioneerName($row["pensioneer_id"]);
      $loan_no = $row["loan_num"];
  }
  
  $check_num = $get_or_details['check_num'];
  if($check_num==0){
    $check_num="";
    $td_check ="<td class='text-bold' width='15%'>&nbsp;[&nbsp;&nbsp;] Check No. </td>";
    $td_cash ="<td class='text-bold' width='15%'><span>&#10003;</span> Cash</td>";
  }else{
    $check_nun=$get_or_details['check_num'];
    $td_check ="<td class='text-bold' width='15%'><span>&#10003;</span> Check No. </td>";
    $td_cash ="<td class='text-bold' width='15%'>&nbsp;[&nbsp;&nbsp;] Cash</td>";
  }

  function getBankName($id){
    $row = mysqli_fetch_array(mysqli_query($conn, "SELECT bank_name FROM tbl_bank WHERE bank_id = '$id'"));
    return $row[0];
  }

  function getPensioneerName($id){
    $row = mysqli_fetch_array(mysqli_query($conn, "SELECT b_fname, b_lname FROM tbl_borrowers WHERE borrower_id = '$id'"));
    return ucfirst($row[1]).", ".ucfirst($row[0]);
  }

  $comp_name = mysqli_fetch_array(mysqli_query($conn, "SELECT global_name FROM tbl_globals"));

  $payment_sum = number_format($amount_payment,2);
  $explode_sum = explode(".", $payment_sum);

  if($explode_sum[1] != '00'){

    $int_word = convertNumberToWord($explode_sum[0]);
    $dec_word = convertNumberToWord($explode_sum[1]);
    $final_word = $int_word." and ".$dec_word." Centavos";
  }else{
    $int_word = convertNumberToWord($explode_sum[0]);
    $final_word = $int_word;
  }

function convertNumberToWord($num = false)
{
    $num = str_replace(array(',', ' '), '' , trim($num));
    if(! $num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven',
        'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'
    );
    $list2 = array('', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety', 'Hundred');
    $list3 = array('', 'Thousand', 'Million', 'Billion', 'Trillion');

    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ( $tens < 20 ) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}

?>

<link rel="stylesheet" href="../../assets/dist/css/adminlte.min.css">
<link rel="stylesheet" href="../../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<link rel="stylesheet" href="../../assets/plugins/datatables/dataTables.bootstrap4.css">
<link rel="stylesheet" media="print" href="../../assets/dist/css/adminlte.min.css">
<link rel="stylesheet" media="print" href="../../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<style type="text/css">
  .wrapper, body, html {
    min-height: 1200px !important;
    padding-right: 1rem;
    padding-left: 1rem;
  }
  .header-td{
    width: 33.33%;
    padding-left: 55px;
  }
  .header-td2{
    width: 20%;
    text-align: center;
  }
  .grid-container {
    display: inline-grid;
    grid-template-columns: auto auto auto;
  }
  .pl-3{
    padding-left: 3rem;
  }
  td, th, tr{
    /*border: 1px solid;*/
    padding-top: .5rem;
  }
  .text-bold{
    font-weight: bold;
  }
</style>
<div class="wrapper" style="height: 1200px;">
  <center>
    <h3 style="margin-top: 5rem">LOAN RECEIPT</h3>
  </center>
  <table style="margin-top: 1rem;" width="100%">
    <tr>
      <td class="text-bold" width="12%">Received from </td>
      <td width="50%" style="border-bottom: 1px solid;  padding-left: 5px;"><?php echo $pensioneer_id;?></td>
      <td class="text-bold" width="10%" style="text-align: right; padding-right: 1rem;">Date</td>
      <td width="20%" style="border-bottom: 1px solid; text-align: center;"><?php echo date("F d, Y", strtotime($now));?></td>
    </tr>
  </table>
  <table width="100%">
    <tr>
      <td class="text-bold" width="10%">The sum of </td>
      <td width="60%" style="border-bottom: 1px solid; padding-left: 5px;"><?php echo $final_word;?> Pesos</td>
      <td class="text-bold" width="10%">Only. <span class="float-right">-P-</span></td>
      <td width="20%" style="border-bottom: 1px solid; padding-left: 1rem;"><?php echo $payment_sum; ?></td>
    </tr>
  </table>
  <table width="100%">
    <tr>
      <td class="text-bold" width="20%">For the payment of loan No. </td>
      <td width="20%" style="border-bottom: 1px solid; padding-left: 1rem;"><?php echo $loan_no;?></td>
      <td width="20%"></td>
      <td  width="20%"></td>
  </tr>
</table>
  <table width="100%" border="1" style="padding: 5px; margin-top: 10px">
  					<tr style="padding: 5px;"">
			        <td class="text-bold" width="10%" style="padding: 10px">Ref. No.</td>        	
			        <td class="text-bold" width="20%" style="padding: 10px">Due date</td>
			        <td class="text-bold" width="15%" style="padding: 10px">Penalty</td>
			        <td class="text-bold" width="15%" style="padding: 10px">Principal</td>
			        <td class="text-bold" width="15%" style="padding: 10px">Interest</td>
			        <td class="text-bold" width="20%" style="padding: 10px">Amount Due</td>
			        </tr>
    <?php
    	$counter = 1;
 		$sql = mysqli_query("SELECT a.subsidiary_id,sum(b.penalty_amount) as penalty,a.principal,a.interest,a.date_added from `tbl_subsidiary` a left join tbl_payment_penalty b on a.subsidiary_id=b.subsidiary_id where a.issued_or_id = '$id' group by a.subsidiary_id")or die(mysqli_error());
 		$sql_count = mysqli_num_rows($sql);
        while($row = mysqli_fetch_array($sql)){

        	
		    
		        if($counter==$sql_count){
		        	    ?>
			        <tr style="margin: 5px;">
			        <td width="10%" style="padding: 10px"><?php echo $row["subsidiary_id"]; ?></td>        	
			        <td width="20%" style="padding: 10px"><?php echo date("F d, Y",strtotime($row["date_added"])).$pen_details;?></td>
			        <td width="15%" style="padding: 10px"><?php if($row["penalty"]==null){echo "0";}else{echo $row["penalty"];}?></td>  
			        <td width="15%" style="padding: 10px"><?php echo $row["principal"];?></td>  
			        <td width="15%" style="padding: 10px"><?php echo $row["interest"];?></td>  
			        <td width="20%" style="padding: 10px">P <?php echo $row["interest"]+$row["principal"]+$row["penalty"]; ?></td>
			        </tr>
		        

		        <?php

		        }else{

		        	    ?>
			        <tr style="margin: 5px; border-bottom-style: hidden;">
			        <td width="10%" style="padding: 10px"><?php echo $row["subsidiary_id"]; ?></td>        	
			        <td width="20%" style="padding: 10px"><?php echo date("F d, Y",strtotime($row["date_added"])).$pen_details;?></td>
			        <td width="15%" style="padding: 10px"><?php if($row["penalty"]==null){echo "0";}else{echo $row["penalty"];}?></td>  
			        <td width="15%" style="padding: 10px"><?php echo $row["principal"];?></td>  
			        <td width="15%" style="padding: 10px"><?php echo $row["interest"];?></td>  
			        <td width="20%" style="padding: 10px">P <?php echo $row["interest"]+$row["principal"]+$row["penalty"]; ?></td>
			        </tr>
		        

		        <?php
		        }

        	
             $counter++;
          }

    ?>
    <tr style="padding: 5px;"">
			        <td class="text-bold" width="10%" style="padding: 10px"></td>        	
			        <td class="text-bold" width="20%" style="padding: 10px"></td>
			        <td class="text-bold" width="15%" style="padding: 10px"></td>
			        <td class="text-bold" width="15%" style="padding: 10px"></td>
			        <td class="text-bold" width="15%" style="padding: 10px">Total:</td>
			        <td class="text-bold" width="20%" style="padding: 10px">P <?php echo $amount_payment;?></td>
			        </tr>

  
  </table>
  <table width="100%" style="margin-top: .5rem;">
    <tr>
      <td width="20%"></td>
      <td class="text-bold" width="55%" style="text-align: right; padding-right: 5px"> Paid By</td>
       <?php echo $td_cash;?>
    </tr>
    <tr>
      <td width="20%"></td>
      <td class="text-bold" width="55%"></td>
      <?php echo $td_check;?>
      <td width="12%" style="border-bottom: 1px solid; padding-left:5px"><?php echo $check_num;?></td>
    </tr>
  </table>
  <table width="100%" style="margin-top: 1rem;">
    <tr>
      <td class="text-bold" width="20%" style="text-align: right; padding-right: .5rem;">Received By: </td>
      <td width="20%" style="border-bottom: 1px solid;"></td>
      <td width=""></td>
    </tr>
  </table>
</div>