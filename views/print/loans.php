<?php 

  include '../../core/conn.php';

  $id = $_GET["id"];

  $sql = mysqli_query($conn, "SELECT * FROM tbl_loan WHERE loan_id = '$id'")or die(mysqli_error());
  $row = mysqli_fetch_array($sql);

  function getBankName($id){
    global $conn;
    $row = mysqli_fetch_array(mysqli_query($conn, "SELECT bank_name FROM tbl_bank WHERE bank_id = '$id'"));
    return isset($row[0]) && $row[0] != ""?$row[0]:"N.A.";
  }

  function getPensioneerName($id){
    global $conn;
    $row = mysqli_fetch_array(mysqli_query($conn, "SELECT b_fname, b_lname FROM tbl_borrowers WHERE borrower_id = '$id'"));
    return $row[0]." ".$row[1];
  }

  $comp_name = mysqli_fetch_array(mysqli_query($conn, "SELECT global_name FROM tbl_globals"));

  ?>
<link rel="stylesheet" href="../../assets/dist/css/adminlte.min.css">
<link rel="stylesheet" href="../../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<link rel="stylesheet" href="../../assets/plugins/datatables/dataTables.bootstrap4.css">
<link rel="stylesheet" media="print" href="../../assets/dist/css/adminlte.min.css">
<link rel="stylesheet" media="print" href="../../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<style type="text/css">
  .wrapper, body, html {
    min-height: 1200px !important;
    padding-right: 1rem;
    padding-left: 1rem;
  }
  .header-td{
    width: 33.33%;
    padding-left: 55px;
  }
  .header-td2{
    width: 20%;
    text-align: center;
  }
  .grid-container {
    display: inline-grid;
    grid-template-columns: auto auto auto;
  }
  .pl-3{
    padding-left: 3rem;
  }
  /*tr, td{
    border: 1px solid;
  }*/
</style>
<div class="wrapper" style="height: 1200px;">
  <center>
    <h1 style="margin-top: 5rem">LETTER OF TRANSMITTAL</h1>
  </center>
  <table style="margin-top: 3rem;" width="100%">
    <tr>
      <td class="header-td"></td>
      <td></td>
      <td class="header-td">#<?php echo $row["loan_no"];?></td>
    </tr>
    <tr>
      <td class="header-td"></td>
      <td></td>
      <td class="header-td">BANK - <?php echo getBankName($row["bank_id"]);?></td>
    </tr>
    <tr>
      <td class="header-td">PAY TO - <?php echo strtoupper(getPensioneerName($row["pensioneers_id"]));?></td>
      <td></td>
      <td class="header-td">DATE - <?php echo date("F d, Y", strtotime($row["date"]));?></td>
    </tr>
  </table>

  <table width="100%">
    <tr style="border: 1px solid;">
      <td class="header-td2">PROMISORY NOTE</td>
      <td class="header-td2">INTEREST</td>
      <td class="header-td2">RATE</td>
      <td class="header-td2">TERM</td>
      <td colspan="2" class="header-td2">GROSS CASH OUT</td>
    </tr>
    <tr>
      <td class="header-td2"><?php echo number_format($row["p_note"],2);?></td>
      <td class="header-td2"><?php echo number_format($row["interest"],2);?></td>
      <td class="header-td2"><?php echo number_format($row["rate"],2);?></td>
      <td class="header-td2"><?php echo $row["term"];?></td>
      <td>-P-</td>
      <td style="text-align: right;"><?php echo number_format($row["gross_cash_out"],2);?></td>
    </tr>
    <tr>
      <td class="header-td2">CHANGE</td>
      <td class="header-td2"></td>
      <td class="header-td2">C. ADV.</td>
      <td class="header-td2"></td>
      <td>-P-</td>
      <td></td>
    </tr>
  </table>

  <table width="100%">
    <tr>
      <td colspan="5" style="padding-left: 2rem; padding-top: .5rem;"><b><i>LESS</i></b></td>
    </tr>

    <!-- FEES -->
    <?php
      $get_loan_deductions = mysqli_query($conn, "SELECT * FROM tbl_loan_deductions WHERE loan_id = '$id'") or die(mysqli_error());
      $total_fees = 0;
      while($ld_data = mysqli_fetch_array($get_loan_deductions)){
      $f_data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_fees WHERE fees_id = '$ld_data[fee_id]'"));
      $total_fees += $f_data["value"];
    ?>
    <tr>
      <td width="20%"></td>
      <td width="20%"><?php echo $f_data["fees_name"];?></td>
      <td width="20%"></td>
      <td width="20%"><?php echo number_format($f_data["value"],2);?></td>
      <td width="20%"></td>
    </tr>
    <?php } ?>
    <!-- FEES END -->
   
    <tr>
      <td colspan="4"></td>
      <td style="border-bottom: 1px solid; width: 20%; text-align: right;"><span style="padding-left: 3rem;"><?php echo number_format($total_fees,2);?></span></td>
    </tr>
    <tr>
      <td colspan="3"></td>
      <td style="text-align: center;">N E T&nbsp;&nbsp;P R O C E E D S</td>
      <td style="border-bottom: 1px solid; width: 20%; text-align: right;"><span style="padding-left: 3rem;"><?php echo number_format($row["net_proceeds"],2);?></span></td>
    </tr>
  </table>

  <table width="100%">
    <tr>
      <td colspan="5" style="padding-left: 55px;">
        Received from <?php echo $comp_name[0]; ?> check <?php echo getBankName($row["bank_id"]);?> for -P- **** <?php echo number_format($row["net_proceeds"],2);?> as full payment of the above.
      </td>
    </tr>
    <tr>
      <td style="text-align: center; padding-top: 2rem;">Prepared By:</td>
      <td style="text-align: center; padding-top: 2rem;">Checked By:</td>
      <td style="text-align: center; padding-top: 2rem;">Noted By:</td>
      <td style="text-align: center; padding-top: 2rem;">Approved By:</td>
      <td style="text-align: center; padding-top: 2rem;">Received By:</td>
    </tr>
    <tr>
      <td style="text-align: center; padding-top: 3rem;">__________________</td>
      <td style="text-align: center; padding-top: 3rem;">__________________</td>
      <td style="text-align: center; padding-top: 3rem;">__________________</td>
      <td style="text-align: center; padding-top: 3rem;">__________________</td>
      <td style="text-align: center; padding-top: 3rem;">__________________</td>
    </tr>
  </table>
</div>