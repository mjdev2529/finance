
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Forecasting</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item active">Forecasting</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                    <div class="col-md-12">
                        <form id="forecasting_form" method="POST" action="">
                          <div class="row">
                            <div class="col-md-4 offset-md-2">
                              <span>Start Date:</span>
                              <input type="date" name="str_date" class="form-control" value="<?php if(isset($_POST['str_date'])){ echo date("Y-m-d", strtotime($_POST['str_date']));}else{ echo date("Y-m-01"); }?>">
                            </div>
                            <div class="mb-3 col-md-4">
                              <span>End Date:</span>
                              <input type="date" name="end_date" class="form-control" value="<?php if(isset($_POST['end_date'])){ echo date("Y-m-d", strtotime($_POST['end_date']));}else{ echo date("Y-m-t"); }?>">
                            </div>
                            <div class="col-md-2 pt-4">
                              <button type="submit" class="btn btn-secondary" onclick="load()"><i class="fa fa-sync mr-1"></i> Generate</button>
                            </div>
                          </div>
                        </form>
                    </div>
                    <hr>
                    <center class="loader" style="display: none;">
                      <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                      <br>
                      Loading please wait...
                    </center>
                     <?php 
                          if(isset($_POST["str_date"]) && isset($_POST["end_date"])){
                            $str = date("Y-m-d H:i:s", strtotime($_POST["str_date"]));
                            $end = date("Y-m-d H:i:s", strtotime($_POST["end_date"]));

                            $sub_sql = mysqli_query($conn, "SELECT * FROM tbl_subsidiary as s, tbl_loan as l WHERE s.loan_id = l.loan_id AND s.date_added BETWEEN '$str' AND '$end' AND l.status != 2");
                            $checker = mysqli_num_rows($sub_sql);

                            if($checker > 0){
                      ?>
                    <table id="tbl_forecasting" class="table table-sm table-stripped">
                      <thead class="bg-dark">
                        <th>#</th>
                        <th>Loan #</th>
                        <th>Date</th>
                        <th>Pensioneer</th>
                        <th>Principal</th>
                        <th>Interest</th>
                        <th>Subtotal</th>
                      </thead>
                      <tbody>
                        <?php
                            $result = mysqli_query($conn, "SELECT * FROM tbl_subsidiary as s, tbl_loan as l WHERE s.loan_id = l.loan_id AND s.date_added BETWEEN '$str' AND '$end' AND l.status != 2")or die(mysqli_error());
                            $count = 1;
                            $total_sum = 0;
                            $total_principal = 0;
                            $total_interest = 0;
                            while($row = mysqli_fetch_array($result)){
                            $sum = $row["principal"] + $row["interest"];
                            $total_sum += $sum;
                            $total_principal += $row["principal"];
                            $total_interest += $row["interest"];
                        ?>
                        <tr>
                          <td><?php echo $count++; ?>.</td>
                          <td><?php echo $row["loan_num"]; ?></td>
                          <td><?php echo date("F d, Y", strtotime($row["date_added"])); ?></td>
                          <td><?php echo get_pensioneer_name($row["pensioneer_id"], $conn); ?></td>
                          <td><?php echo number_format($row["principal"],2); ?></td>
                          <td><?php echo number_format($row["interest"],2); ?></td>
                          <td><?php echo number_format($row["principal"] + $row["interest"],2); ?></td>
                        </tr>
                        <?php } ?>
                          <tr style="border-top: 2px solid;">
                            <td colspan="4"><b>TOTAL :</b></td>
                            <td><?php echo number_format($total_principal,2);?></td>
                            <td><?php echo number_format($total_interest,2);?></td>
                            <td><?php echo number_format($total_sum,2);?></td>
                          </tr>
                      </tbody>
                    </table>
                <?php }else{ ?>
                	<h4 class="text-center inform" style="display: none;"><i class="fa fa-info-circle"> No Data Available.</i></h4>
                <?php } }?>
                  </div>
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
      function load(){
        $(".loader").show();
        $("#tbl_forecasting").hide();
        $(".inform").hide();
        setTimeout( function(){
          $(".loader").hide();
          $("#tbl_forecasting").show();
          $(".inform").show();
        },1500);
      }
    </script>