<?php 

  // include '../../core/conn.php';

  // $id = $_GET["id"];

  // $sql = mysql_query("SELECT * FROM tbl_loan WHERE loan_id = '$id'")or die(mysql_error());
  // $row = mysql_fetch_array($sql);

  // function getBankName($id){
  //   $row = mysql_fetch_array(mysql_query("SELECT bank_name FROM tbl_bank WHERE bank_id = '$id'"));
  //   return $row[0];
  // }

  // function getPensioneerName($id){
  //   $row = mysql_fetch_array(mysql_query("SELECT fname, lname FROM tbl_pensioneers WHERE pensioneer_id = '$id'"));
  //   return $row[0]." ".$row[1];
  // }

  // $comp_name = mysql_fetch_array(mysql_query("SELECT global_name FROM tbl_globals"));

  ?>
<link rel="stylesheet" href="../assets/dist/css/adminlte.min.css">
<link rel="stylesheet" href="../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<link rel="stylesheet" href="../assets/plugins/datatables/dataTables.bootstrap4.css">
<link rel="stylesheet" media="print" href="../assets/dist/css/adminlte.min.css">
<link rel="stylesheet" media="print" href="../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<style type="text/css">
  .wrapper, body, html {
    min-height: 1200px !important;
    padding-right: 1rem;
    padding-left: 1rem;
  }
  .header-td{
  	width: 33.33%;
  	padding-left: 55px;
  }
  .header-td2{
  	width: 20%;
  	text-align: center;
  }
  .grid-container {
	  display: inline-grid;
	  grid-template-columns: auto auto auto;
	}
	.pl-3{
		padding-left: 3rem;
	}
</style>
<div class="wrapper" style="height: 1200px;">
	<center>
		<h1 style="margin-top: 5rem">LETTER OF TRANSMITTAL</h1>
	</center>
	<table style="margin-top: 5rem;" width="100%">
		<tr>
			<td class="header-td"></td>
			<td></td>
			<td class="header-td">#1</td>
		</tr>
		<tr>
			<td class="header-td"></td>
			<td></td>
			<td class="header-td">BANK - BPI</td>
		</tr>
		<tr>
			<td class="header-td">PAY TO - John Doe</td>
			<td></td>
			<td class="header-td">DATE - September 19, 2019</td>
		</tr>
	</table>

	<table width="100%">
		<tr style="border: 1px solid;">
			<td class="header-td2">PROMISORY NOTE</td>
			<td class="header-td2">INTEREST</td>
			<td class="header-td2">RATE</td>
			<td class="header-td2">TERM</td>
			<td colspan="2" class="header-td2">GROSS CASH OUT</td>
		</tr>
		<tr>
			<td class="header-td2">50,000.00</td>
			<td class="header-td2">15,000.00</td>
			<td class="header-td2">2.50</td>
			<td class="header-td2">12</td>
			<td class="">
				<div class="grid-container">
					<span class="pl-3">-P- </span>
					<span class="pl-3">35,000.00</span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="header-td2">CHANGE</td>
			<td class="header-td2"></td>
			<td class="header-td2">C. ADV.</td>
			<td class="header-td2"></td>
			<td class="">
				<div class="grid-container">
					<span class="pl-3">-P- </span>
					<span class="pl-3">35,000.00</span>
				</div>
			</td>
		</tr>
		<!-- FEES -->
		<tr>
			<td></td>
			<td>ID</td>
			<td></td>
			<td>50.00</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Collection Fee</td>
			<td></td>
			<td>10.00</td>
			<td></td>
		</tr>
		<!-- FEES END -->
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><span style="width: 400px; border-bottom: 1px solid;">60.00</span></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td style="text-align: center;">N E T&nbsp;&nbsp;P R O C E E D S</td>
			<td><span style="width: 400px; border-bottom: 1px solid;">34,940.00</span></td>
		</tr>
		<tr>
			<td colspan="5" style="padding-left: 55px;">
				Received from Test Company check BPI for -P- **** 34,940.00 as full payment of the above.
			</td>
		</tr>
		<tr>
			<td style="text-align: center; padding-top: 2rem;">Prepared By:</td>
			<td style="text-align: center; padding-top: 2rem;">Checked By:</td>
			<td style="text-align: center; padding-top: 2rem;">Noted By:</td>
			<td style="text-align: center; padding-top: 2rem;">Approved By:</td>
			<td style="text-align: center; padding-top: 2rem;">Received By:</td>
		</tr>
		<tr>
			<td style="text-align: center; padding-top: 3rem;">__________________</td>
			<td style="text-align: center; padding-top: 3rem;">__________________</td>
			<td style="text-align: center; padding-top: 3rem;">__________________</td>
			<td style="text-align: center; padding-top: 3rem;">__________________</td>
			<td style="text-align: center; padding-top: 3rem;">__________________</td>
		</tr>
	</table>
</div>