<?php
  $id = $_GET["id"];

  $get_or_details = mysqli_fetch_array(mysqli_query($conn, "SELECT * from tbl_official_receipt where or_id = '$id'"));
  $partial_payment_id = $get_or_details['partial_payment_id'];
  if($partial_payment_id>0){
 $get_tbl_partial = mysqli_fetch_array(mysqli_query($conn, "SELECT subsidiary_id from tbl_partial_payment where or_id = '$id'"));
      $subsidiary_id = $get_tbl_partial['subsidiary_id'];
      $sql = mysqli_query($conn, "SELECT * FROM tbl_subsidiary WHERE subsidiary_id = '$subsidiary_id'")or die(mysqli_error());
      $get_loan_num = mysqli_fetch_array($sql);
  $loan_num = $get_loan_num['loan_num'];
  $loan_id = $get_loan_num['loan_id'];
  }else{

  $sql = mysqli_query($conn, "SELECT * FROM tbl_subsidiary WHERE issued_or_id = '$id'")or die(mysqli_error());
  $get_loan_num = mysqli_fetch_array($sql);
  $loan_num = $get_loan_num['loan_num'];
  $loan_id = $get_loan_num['loan_id'];
  }

?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Official Receipt Print <small><br>( For Payment of Loan #: <?php echo $loan_num;?>)</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
          <li class="breadcrumb-item"><a href="index.php?page=add-official-receipt&id=<?php echo $loan_id; ?>">Official Receipt</a></li>
          <li class="breadcrumb-item active">Official Receipt Print</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="btn-group float-right mb-3">
              <button type="button" class="btn btn-secondary" onclick="printFrame()"><i class="fas fa-print"></i> Print</button>
            </div>
            <div class="dropdown-divider col-md-12"></div>
            <iframe id="OR_frame" src="print/official_receipt.php?id=<?php echo $id;?>" width="100%" style="height: 1344px;"></iframe>
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </div><!--/. container-fluid -->
</section>
<!-- /.content -->

<script type="text/javascript">
	function printFrame() {
            var frm = document.getElementById("OR_frame").contentWindow;
            frm.focus();// focus on contentWindow is needed on some ie versions
            frm.print();
            return false;
	}
</script>