<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Official Receipt</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('home')?>">Home</a></li>
          <li class="breadcrumb-item active">Official Receipt</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
              <div class="col-sm-12 mb-2">
                <div class="row">
                  <div class="col-sm-4 offset-sm-2">
                    <span>Start Date:</span>
                    <input id="start_date" type="date" class="form-control" value="<?php  echo get_first_day_current_month();?>">
                  </div>
                  <div class="col-sm-4">
                    <span>End Date:</span>
                    <input id="end_date" type="date" class="form-control" value="<?php echo get_last_day_current_month();?>">
                  </div>
                  <div class="col-sm-2 pt-4">
                    <button type="button" href="#" class="btn btn-secondary" onclick="get_data()"><i class="fas fa-sync-alt"></i> Generate</button>
                  </div>
                </div>
              </div>

            <div class="dropdown-divider col-md-12 mt-3 mb-3"></div>

            <table id="tbl_loans" class="table table-sm table-bordered table-striped">
              <thead>
              <tr class="bg-dark">
                <th width="20px;">#</th>
                <th width="80px"></th>
                <th width="100px;">Loan #</th>
                <th width="150px;">Date</th>
                <th>Pensioner</th>
                <th width="110">Status</th>
              </tr>
              </thead>
              <tbody>
              </tbody>                
            </table>
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </div><!--/. container-fluid -->
</section>
<!-- /.content -->
<?php require "template/modals/viewLoans_MD.php";?>
<script type="text/javascript">

  $(document).ready( function(){
    $("#tbl_loans").dataTable();
    get_data();
  });

  function checkAll(){
    var x = $("#checkAll").is(":checked");
    if(x){
      $("input[name=loans]").prop("checked", true);
    }else{
      $("input[name=loans]").prop("checked", false);
    }
  }

  function viewLoan(id){
    var url = "../ajax/loan_details.php";
    $.post(url,{id: id},function(data){
      $(".loan_body").html(data);
      $("#modal_view_loans").modal();
    });
  }

  function delete_loan(){

    var url = "../ajax/delete_loan.php";
    var x  = confirm("Are you sure to delete?");
    var checkedVals = [];

    $('input[name=loans]:checked').map(function() {
      checkedVals.push($(this).val());
    });

    if(x){
      if(checkedVals){
        $.post(url,{id: checkedVals},function(data){
          if(data == 1){
            iziAlert("fa fa-check","Success! ,","Selected loan deleted.","bottomLeft","success");
            get_data();
          }else{
           iziAlert("fa fa-ban","Error! ,","Something was wrong.","bottomLeft","error");
          }
        });
      }else{
        iziAlert("fa fa-info","Warning! ,","No loan selected.","bottomLeft","warning");
      }
    }
   
    }

	function test(){
      $.ajax({
          type: "POST",
          url: '../ajax/datatables/loans.php',
          data: {"type":"check"},
         success: function(response){
            alert(response);
          }
     });
	}

  function get_data(){
   
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    
    $("#tbl_loans").DataTable().destroy();
    $('#tbl_loans').DataTable({
      "processing":true,
      "order": [[ 0, "asc" ]],
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/loans.php",
        "dataSrc":"data",
        "data":{
          start_date:start_date,
          end_date:end_date
        }
      },
      "columnDefs":[
        {targets:[1],orderable: false}
      ],
      "columns":[
        {
          "data":"count"
        },
        {
          "mRender": function(data,type,row){
            if(row.stat != 2 && row.net_proceeds != "0.000"){
              return "<a class='text-sm btn btn-outline-dark btn-sm' href='index.php?page=add-official-receipt&id="+row.id+"'>"+
                        "Issue Receipt"+
                      "</a>";
            }else{
              return  "<i class='fa fa-ban ml-2'></i>";
            }
          }
        },
        {
          "data":"loan_no"
        },
        {
          "data":"date"
        },
        {
          "data":"pensioneers_name"
        }
        ,
        {
          "data":"status"
        }
      ]
    });
  }
</script>