
	function iziAlert(icon,title,msg,pos,type){
		if(type == "success"){
			iziToast.success({
				icon: icon,
			    title: title,
			    message: msg,
			    position: pos,
			    timeout: 2000,
			    close: true,
			    closeOnClick: true
		   	});
		}else if(type == "error"){
			iziToast.error({
		    	icon: icon,
			    title: title,
			    message: msg,
			    position: pos,
			    timeout: 2000,
			    close: true,
			    closeOnClick: true
		   	});
		}else if(type == "warning"){
			iziToast.warning({
		     	icon: icon,
			    title: title,
			    message: msg,
			    position: pos,
			    timeout: 2000,
			    close: true,
			    closeOnClick: true
		   	});
		}else{
			iziToast.info({
		     	icon: icon,
			    title: title,
			    message: msg,
			    position: pos,
			    timeout: 2000,
			    close: true,
			    closeOnClick: true
		   	});
		}
	}
