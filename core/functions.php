<?php

  function page_url($page){
    return md5(base64_encode($page));
  }

 function enCrypt($data){
 	return base64_encode($data);
 }

 function deCrypt($data){
 	return base64_decode($data);
 }

 function page_title($page){
  //tst = TRANSACTIONS TREE
  //rpt = REPORTS TREE
  //mtct = MAINTENANCE TREE
  //msct = MISCELLANEOUS TREE
  
  if($page == page_url('home')){
    $title = "Dashboard";
    $active = "active bg-white";
  }elseif ($page == page_url('banks')) {
     $title = "Bank";
     $activeBank = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }elseif ($page == page_url('loans')) {
     $title = "Loans";
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('add-loan')) {
     $title = "Add Loan";
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('view-loan')) {
     $title = "Loan Details";
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('settings')) {
     $title = "Global Settings";
     $activeGS = "active bg-white";
     $msct = "menu-open";
     $active = "";
  }elseif ($page == page_url('subsidiary')) {
     $title = "Subsidiary";
     $activeSUB = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }elseif ($page == page_url('deductions')) {
     $title = "Global Deductions";
     $activeGD = "active bg-white";
     $msct = "menu-open";
     $active = "";
  }elseif ($page == page_url('official-receipt')) {
     $title = "Official Receipt";
     $activeOR = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('add-official-receipt')) {
     $title = "Add Official Receipt";
     $activeOR = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('loan-print')) {
     $title = "Loan Print";
     $activeLoan = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('print-official-receipt')) {
     $title = "Official Receipt Print";
     $activeOR = "active bg-white";
     $tst = "menu-open";
     $active = "";
  }elseif ($page == page_url('forecasting')) {
     $title = "Forecasting";
     $activeFC = "active bg-white";
     $rpt = "menu-open";
     $active = "";
  }elseif ($page == page_url('daily-collection')) {
     $title = "Daily Collection";
     $activeDC = "active bg-white";
     $rpt = "menu-open";
     $active = "";
  }elseif ($page == page_url('daily-LOT')) {
     $title = "Daily LOT";
     $activeDL = "active bg-white";
     $rpt = "menu-open";
     $active = "";
  }elseif ($page == page_url('users')) {
     $title = "User Management";
     $activeUsers = "active bg-white";
     $msct = "menu-open";
     $active = "";
  }elseif ($page == page_url('member-type')) {
     $title = "Type of Member";
     $activeMemType = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }elseif ($page == page_url('borrowers')) {
     $title = "Borrowers";
     $activeMems = "active bg-white";
     $mtct = "menu-open";
     $active = "";
  }else{
    $title = "404 Page not found";
    $active = "";
  }

  return $title;
 }

  function get_pensioneer_name($id, $conn){
    global $conn;
  	$get_name = mysqli_fetch_assoc(mysqli_query($conn, "SELECT b_fname,b_lname from tbl_borrowers where borrower_id = '$id'"));
  	$name = ucfirst($get_name['b_lname']).", ".ucfirst($get_name['b_fname']);
 	return $name;
 }

   function get_pensioneer_name_by_subsidiary_id($id){
    $get_name = mysqli_fetch_assoc(mysqli_query($conn, "SELECT b.b_fname,b.b_lname FROM `tbl_subsidiary` a inner join tbl_borrowers b on a.pensioneer_id=b.borrower_id where a.subsidiary_id = '$id'"));
    $name = ucfirst($get_name['b_lname']).", ".ucfirst($get_name['b_fname']);
  return $name;
 }

 function getCheckNum(){
  global $conn;
 	$GBSettings = mysqli_fetch_array(mysqli_query($conn, "SELECT check_num FROM tbl_globals"));
 	$getUsedChecks = mysqli_fetch_array(mysqli_query($conn, "SELECT count(check_no) as used_check, max(check_no) as current_check FROM tbl_loan"));

 	$used_check1 = $GBSettings["check_num"] + $getUsedChecks["used_check"];

 	if($getUsedChecks["used_check"] == 0){
 		$check_no_sum = $GBSettings["check_num"];
 	}else if($used_check1 == $getUsedChecks["current_check"]){
 		$check_no_sum = $used_check1 - 1;
 	}else{
 		$check_no_sum = $used_check1;
 	}

	return $check_no_sum;
 }

 function getORNum(){
  global $conn;
	$GBSettings = mysqli_fetch_array(mysqli_query($conn, "SELECT or_num FROM tbl_globals"));
	$getUsedChecks = mysqli_fetch_array(mysqli_query($conn, "SELECT or_num+1 as or_num FROM `tbl_official_receipt` ORDER BY or_id DESC LIMIT 0, 1"));

	if(!isset($getUsedChecks["or_num"]) || $getUsedChecks["or_num"] == 0){
		$or_no_sum = $GBSettings["or_num"];
    return $or_no_sum;
	}else{
		$or_no_sum = $getUsedChecks["or_num"];
    return $or_no_sum;
	}
 }

 function get_pensioneer_id($id){
    global $conn;
 		$get_name = mysqli_fetch_assoc(mysqli_query($conn, "SELECT pensioneers_id from tbl_loan where loan_id = '$id'"));
  	$pensioneers_id = $get_name['pensioneers_id'];
 	return $pensioneers_id;
 }

 function get_pensioneer_code($id){
  global $conn;
 	$get_code = mysqli_fetch_array(mysqli_query($conn, "SELECT b_code from tbl_borrowers where borrower_id = '$id'"));
 	return $get_code[0];
 }

function get_current_date(){
	ini_set('date.timezone','UTC');
    date_default_timezone_set('UTC');
    // $today = date('Y-m-d');
    $date_today = date('Y-m-d');
 
    return  $date_today;
}

function get_first_day_current_month(){
  ini_set('date.timezone','UTC');
    date_default_timezone_set('UTC');
    // $today = date('Y-m-d');
    $date_today = date('Y-m-d');

    return date('Y-m-01', strtotime($date_today));
}

function  get_last_day_current_month(){
  ini_set('date.timezone','UTC');
    date_default_timezone_set('UTC');
    // $today = date('Y-m-d');
    $date_today = date('Y-m-d');
    return date('Y-m-t', strtotime($date_today));
}

?>