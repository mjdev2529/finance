 <?php
	define ('path','../views/');

	if($page == page_url('home')){
		// if($_SESSION['role'] == "R"){
		// 	require path.'home_root.php';
		// }else{
			require path.'home.php';
		// }
	}else if($page == page_url('users')){
			require path.'manage_users.php';
	}else if($page == page_url('loans')){
			require path.'loans.php';
	}else if($page == page_url('banks')){
			require path.'bank.php';
	}else if($page == page_url('add-loan')){
			require path.'add_loans.php';
	}else if($page == page_url('view-loan')){
			require path.'view_loans.php';
	}else if($page == page_url('settings')){
			require path.'settings.php';
	}else if($page == page_url('subsidiary')){
			require path.'loan_subsidiary.php';
	}else if($page == page_url('deductions')){
			require path.'global_deductions.php';
	}else if($page == page_url('official-receipt')){
			require path.'official_receipt.php';
	}else if($page == page_url('add-official-receipt')){
			require path.'add_official_receipt.php';
	}else if($page == page_url('loan-print')){
			require path.'loans_print.php';
	}else if($page == page_url('print-official-receipt')){
			require path.'official_receipt_print.php';
	}else if($page == page_url('forecasting')){
			require path.'forecasting.php';
	}else if($page == page_url('daily-collection')){
			require path.'daily_collection.php';
	}else if($page == page_url('daily-LOT')){
			require path.'daily_LOT.php';
	}else if($page == page_url('member-type')){
			require path.'member_type.php';
	}else if($page == page_url('borrowers')){
			require path.'members.php';
	}else{
		if(!empty($page) or $page != $page){
			require path.'error/error.php';
		}else{
			require path.'home.php';
		}
	}
	
 ?>
