
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Finance System | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="assets/dist/css/iziToast.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<style type="text/css">
  .lds-facebook {
      display: inline-block;
      position: relative;
      width: 64px;
      height: 20px;
      transform: scale(0.5);
    }
    .lds-facebook div {
      display: inline-block;
      position: absolute;
      left: 6px;
      width: 13px;
      background: #fff;
      animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    }
    .lds-facebook div:nth-child(1) {
      left: 6px;
      animation-delay: -0.24s;
    }
    .lds-facebook div:nth-child(2) {
      left: 26px;
      animation-delay: -0.12s;
    }
    .lds-facebook div:nth-child(3) {
      left: 45px;
      animation-delay: 0;
    }
    @keyframes lds-facebook {
      0% {
        top: 6px;
        height: 20px;
      }
      50%, 100% {
        top: 12px;
        height: 20px;
      }
    }
</style>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><b>Finance</b> System</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

     <form id="login_form" action="" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="username" required="">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fa fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password" required="">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fa fa-lock"></span>
            </div>
          </div>
        </div>
          <div class="row">
          <div class="col-12">
            <button type="submit" id="btn-login" class="btn btn-secondary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->

      <p class="mb-1 mt-1">
        <a href="#">I forgot my password</a>
      </p>
    </div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="assets/plugins/js/bootstrap.bundle.min.js"></script>
<script src="assets/dist/js/iziToast.min.js"></script>
<script type="text/javascript">

  $("#login_form").submit( function(e){
    e.preventDefault();
    var url = "ajax/auth.php";
    var data = $(this).serialize();

    $("#btn-login").prop("disabled", true);
    $("#btn-login").html('<div class="lds-facebook"><div></div><div></div><div></div></div>');

    setTimeout( function(){
      $.post(url,data, function(data){
        if(data == 1){
          iziAlert("fa fa-check","Logging in,","Please wait...","bottomLeft","success");
          setTimeout( function(){
            window.location="views/index.php?page=home";
          },2200);
        }else if(data == 2){
          $("#btn-login").html("Sign In");
          $("#btn-login").prop("disabled", false);
          iziAlert("fa fa-times","Warning,","Account was <b>Disabled</b>, Please contact Administrator.","bottomLeft","warning");
        }else{
          $("#btn-login").html("Sign In");
          $("#btn-login").prop("disabled", false);
          iziAlert("fa fa-times","Error,","Username or Password was incorrect.","bottomLeft","error");
        }
      });
    },1000);
  });

<?php include "core/globals_script.php";?>
</script>
</body>
</html>
