<?php 
  include "../core/conn.php"; 
  $id = $_POST["id"];
  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_loan WHERE loan_id = '$id'"));
  $getPensioneer = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_pensioneers WHERE pensioneer_id = '$row[pensioneers_id]'"));
?>
<script type="text/javascript">
  $("#loan_num").html("<?php echo $row["loan_no"]?>");
  var type = "<?php echo $row["method_type"]?>";

  if(type == "76"){
    $("#cb_76").prop("checked", true).prop("disabled", true);
    $("#cb_straight").prop("checked", false).prop("disabled", true);
  }else{
    $("#cb_straight").prop("checked", true).prop("disabled", true);
    $("#cb_76").prop("checked", false).prop("disabled", true);
  }
</script>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active text-dark" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Main</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Loan Entry</a>
          </li>
        </ul>
        <div class="tab-content" id="custom-content-below-tabContent">
          <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
            
            <div class="mt-4 mb-3 row">

              <div class="col-md-12 p-3" style="border: 1px solid; border-radius: 6px;">

                <div class="row">

                  <div class="form-group col-md-12 row">
                    <label for="inputEmail3" class="col-1 control-label">Pay To:</label>
                    <div class="col-4">
                      <div class="form-control">
                        <?php echo $getPensioneer["fname"]." ".$getPensioneer["lname"];?>
                      </div>
                    </div>
                  </div>

                  <div class="dropdown-divider col-md-12 mt-0 mb-1"></div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1">Code #:</label>
                    <div class="form-control">
                      <?php echo $getPensioneer["code_num"];?>
                    </div>
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1">Type:</label>
                    <select class="form-control select2" disabled="">
                      <option value="1" <?php if($row["loan_type"]==1){echo "selected";}?>>Loan</option>
                   </select>
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1">Loan #:</label>
                    <div class="form-control">
                      <?php echo $row["loan_no"];?>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1">Bank:</label>
                    <select class="form-control" name="bank" disabled="">
                      <?php 
                        $getBank = mysqli_query("SELECT * FROM tbl_bank");
                        while($data = mysqli_fetch_array($getBank)){
                      ?>
                        <option <?php if($data['bank_id'] == $row['bank_id']){echo "selected";}?>><?php echo $data['bank_name'];?></option>
                      <?php } ?>
                   </select>
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1">Check #:</label>
                    <div class="form-control">
                      <?php echo $row["check_no"];?>
                    </div>
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1">Date:</label>
                    <div class="form-control">
                      <?php echo date("Y-m-d", strtotime($row["date"]));?>
                    </div>
                  </div>

                  <div class="col-md-3 row">

                    <label class="col-sm-12">Method:</label>

                    <div class="form-group mb-0 col-sm-4 offset-sm-1">
                      <label for="inputPassword3" class="control-label mr-2">
                        <input type="checkbox" id="cb_76">
                      </label>
                      <b id="mt_1">76</b>
                    </div>

                    <div class="form-group mb-0 col-sm-6">
                      <label for="inputPassword3" class="control-label mr-2">
                        <input type="checkbox" id="cb_straight">
                      </label>
                      <b id="mt_2">Straight</b>
                    </div>

                  </div>

                  <div class="dropdown-divider col-md-12 mt-0 mb-1"></div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1" class="text-primary font-weight-normal">Prom. Note</label>
                    <div class="form-control">
                      <?php echo number_format($row["p_note"],2);?>
                    </div>
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1" class="text-primary font-weight-normal">Interest</label>
                    <div class="form-control">
                      <?php echo number_format($row["interest"],2);?>
                    </div>
                  </div>

                  <div class="form-group col-md-3 offset-md-3">
                    <label for="exampleInputEmail1" class="text-primary font-weight-normal">Gross Cash Out</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">-P-</span>
                      </div>
                      <div class="form-control">
                        <?php echo number_format($row["gross_cash_out"],2);?>
                      </div>
                    </div>
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1" class="text-primary font-weight-normal">Rate</label>
                    <div class="form-control">
                      <?php echo number_format($row["rate"],1);?>
                    </div>
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1" class="text-primary font-weight-normal">Term</label>
                    <div class="form-control">
                      <?php echo number_format($row["term"],0);?>
                    </div>
                  </div>

                  <div class="form-group col-md-3 offset-md-3">
                    <label for="exampleInputEmail1">&nbsp;</label>
                    <div class="form-control">0</div>
                  </div>
                  

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1" class="text-primary font-weight-normal">Change:</label>
                    <input type="number" class="form-control" name="change" id="change" value="">
                  </div>

                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail1" class="text-primary font-weight-normal">Cash Advance:</label>
                    <input type="number" class="form-control" name="cash_advance" id="cash_advance">
                  </div>

                  <div class="dropdown-divider col-md-12 mt-0 mb-3"></div>

                  <div class="col-md-12">
                    <label><i>LESS:</i></label>
                  </div>

                  <div class="col-md-9 row pl-5">
                    <?php
                      $getFees = mysqli_query($conn, "SELECT * FROM tbl_fees");
                      $fees_sum = 0;
                      while($fees = mysqli_fetch_array($getFees)){
                        $fees_sum += $fees["value"];
                    ?>
                      <div class="form-group col-md-4">
                        <label for="exampleInputEmail1"><?php echo $fees["fees_name"]; ?>:</label>
                        <div class="form-control"><?php echo number_format($fees["value"],2);?></div>
                      </div>
                    <?php } ?>
                    <div class="form-group col-md-4">
                      <label for="exampleInputEmail1">Others:</label>
                      <div class="form-control">0</div>
                    </div>
                  </div>

                  <div class="dropdown-divider col-md-12 mt-0 mb-3"></div>
                  <?php if($row["old_accounts"] == 1){?>
                    <div class="col-md-8">
                      <label><i><u>TO CLOSE OLD ACCOUNT IF ANY:</u></i></label>

                      <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-5 pl-5 control-label">Old Accounts:</label>
                        <div class="col-sm-3">
                          <input type="number" class="form-control" name="old_accounts" id="old_accounts" value="">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-5 pl-5 control-label">UDI Rebate:</label>
                        <div class="col-sm-3">
                          <input type="number" class="form-control" name="UBI_rebate" id="UBI_rebate" value="">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-5 pl-5 control-label">Coll. Fee Rebate:</label>
                        <div class="col-sm-3">
                          <input type="number" class="form-control" name="coll_rebate" id="coll_rebate" value="">
                        </div>
                      </div>

                    </div>
                  <div class="dropdown-divider col-md-12 mt-0 mb-1"></div>
                  <?php } ?>

                  <div class="form-group col-md-3 offset-md-9">
                    <label for="exampleInputEmail1">&nbsp;</label>
                    <div class="form-control"><?php echo number_format($fees_sum,2);?></div>
                  </div>

                  <div class="form-group col-md-3 offset-md-6 text-right">
                    <span for="exampleInputEmail1"><i class="text-primary">NET PROCEEDS:</i></span>
                  </div>

                  <div class="input-group mb-3 col-md-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">-P-</span>
                    </div>
                    <div class="form-control">
                      <?php echo number_format($row["net_proceeds"],2);?>
                    </div>
                  </div>

                  </div>

                </div>

            </div>

          </div>

          <div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
             ENTRY
          </div>
        </div>
        <!-- <div class="tab-custom-content">
          <p class="lead mb-0">Custom Content goes here</p>
        </div> -->
       </div>
      <!-- /.card -->
    </div>
  </div>
  <!-- /.col -->
</div>