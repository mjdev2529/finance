<?php
include "../core/conn.php";

	// $pensioneer = $_POST['pensioneer'];
	// $code_num = $_POST['code_num'];
	// $type = $_POST['type'];
	// $loan_num = $_POST['loan_num'];
	// $bank = $_POST['bank'];
	// $check_num = $_POST['check_num'];
	// $date = $_POST['date'];
	$loan_id = $_POST['loan_id'];
	$prom_note = $_POST['prom_note'];
	$interest = $_POST['interest'];
	$gross = number_format($_POST['gross'],2,'.','');
	$rate = $_POST['rate'];
	$term = $_POST['term'];
	$d_ins_amt = $_POST["d_ins_amt"];

	//trans type
	// $cb_cash = $_POST['cb_cash'];
	// $cb_check = $_POST['cb_check'];

	// if($cb_check == 1){
	// 	$check_trans = ", bank_id = '$bank', check_no = '$check_num'";
	// }else{
	// 	$check_trans = "";
	// }

	//method type
	// $method_type = "";
	$net_proceeds = number_format($_POST['net_proceeds'],2,'.','');
	$change = number_format($_POST['change'],2,'.','');
	$cash_advance = number_format($_POST['cash_advance'],2,'.','');

	$old_account = $_POST["add_old_acc"];
	
	ini_set('date.timezone','UTC');
  	date_default_timezone_set('UTC');
  	// $today = date('Y-m-d');
  	// $date_today = date('Y-m-d', strtotime($date));
  	// $date_today = date("Y-m-d", strtotime("+1 months", strtotime($now)));

$sql_insert = mysqli_query($conn, "UPDATE tbl_loan SET p_note = '$prom_note', interest = '$interest', rate = '$rate', term = '$term', `gross_cash_out` = '$gross', net_proceeds = '$net_proceeds' WHERE loan_id = '$loan_id'");

// $sql_insert = mysqli_query("INSERT INTO `tbl_loan` (`loan_no`, `code_no`, `date`, `loan_type`, `bank_id`, `check_no`, `pensioneers_id`, `p_note`, `interest`, `rate`, `term`, `gross_cash_out`, `net_proceeds`, `method_type`,`loan_change`,`cash_advance`,`status`,`old_account`) VALUES ('$loan_num', '$code_num', '$date', '$type', '$bank', '$check_num', '$pensioneer', '$prom_note', '$interest', '$rate', '$term', '$gross', '$net_proceeds', '$method_type','$change','$cash_advance','0','$old_account')") or die(mysqli_error());
// $loan_id = mysqli_insert_id();

if($sql_insert){

	$fee_name = mysqli_fetch_array(mysqli_query($conn, "SELECT fees_id FROM tbl_fees WHERE fees_name LIKE '%Insurance%'"));

	$fees_sql = mysqli_query($conn, "INSERT INTO tbl_loan_deductions SET loan_id = '$loan_id', fee_id = '$fee_name[0]', deduction_amount = '$d_ins_amt'");

	$loan_data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_loan WHERE loan_id = '$loan_id'"));
	$pensioneer = $loan_data['pensioneers_id'];
	$loan_num = $loan_data['loan_no'];
	$b_data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_borrowers WHERE borrower_id = '$pensioneer'"));

	$cb_76 = $loan_data['method_type'];

	$loan_date = $loan_data['date'];
	// $month = date("m",strtotime($mydate));
	
	//subsidiary
	if($cb_76=="76"){
		$method_type = "76";
	}else{
		$method_type = "straight";
	}

	if($cb_76=="76"){
		$method_type = "76";

		$xterm = array();
		$sum_deduction = array();
		for ($x = 0; $x < $term; $x++) {
		    $xterm[$x]=$x+1;
		   
		}
		$total_term = array_sum($xterm);
		$principal = round($gross/$term,3);
		$counter = 0;
		$sum_deduction = 0;
		for ($x = $term; $x > 0; --$x) {
			$counter++;
	  		$interest_month = round($interest*($x/$total_term),3);
	    	$total_deduction = round($interest_month+$principal,3);
	     	$beginning_balance = round($prom_note - $sum_deduction,3);
	   		$ending_balance = round($beginning_balance - $total_deduction,3);
	     	$sum_deduction += round($total_deduction,3);	
	     	$m_row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_members_type WHERE mem_type_id = '$b_data[mem_type_id]'"));
	     		$m = date("m",strtotime($loan_date));
	     		$y = date("Y",strtotime($loan_date));
	     		if($m_row["mem_coll_date"] != "0" && $m_row["mem_coll_date2"] != "0"){
	     			$monthly_int = $interest_month / 2;
	     			$prin = $principal / 2;
	     			$end_bal = $ending_balance / 2;
	     		}else{
	     			$monthly_int = $interest_month;
	     			$prin = $principal;
	     			$end_bal = $ending_balance;
	     		}

	     		if($m_row["mem_coll_date"] != "0"){
		     		$date_today = date("Y-m-d", strtotime($y."-".$m."-".$m_row["mem_coll_date"]));
			     	$effectiveDate = date('Y-m-d H:i:s', strtotime("+".$counter." months", strtotime($date_today)));

			     	$insert_subsidiary = mysqli_query($conn, "INSERT INTO `tbl_subsidiary` (`loan_id`, `pensioneer_id`, `loan_num`, `month`, `beginning_balance`, `interest`, `principal`, `ending_balance`, `date_added`, `status`) VALUES ('$loan_id','$pensioneer', '$loan_num', '$counter', '$beginning_balance', '$monthly_int', '$prin', '$end_bal', '$effectiveDate','P')");
		     	}

		     	if($m_row["mem_coll_date2"] != "0"){
			     	$date_today2 = date("Y-m-d", strtotime($y."-".$m."-".$m_row["mem_coll_date2"]));
			     	$effectiveDate2 = date('Y-m-d H:i:s', strtotime("+".$counter." months", strtotime($date_today2)));

			     	$insert_subsidiary2 = mysqli_query($conn, "INSERT INTO `tbl_subsidiary` (`loan_id`, `pensioneer_id`, `loan_num`, `month`, `beginning_balance`, `interest`, `principal`, `ending_balance`, `date_added`, `status`) VALUES ('$loan_id','$pensioneer', '$loan_num', '$counter', '$beginning_balance', '$monthly_int', '$prin', '$end_bal', '$effectiveDate2','P')");
		     	}
		    
		}

		if($insert_subsidiary && $insert_subsidiary2){
			echo 1;
		}else{
			echo 0;
		}
		
	}else{

		$method_type = "straight";
		$principal = round($gross/$term,3);
		$counter = 0;
		$sum_deduction = 0;
		for ($x = $term; $x > 0; --$x) {
					$counter++;
		    
		  		$interest_month = round(($interest/$term),3);
		    	$total_deduction = round($interest_month+$principal,3);
		     	$beginning_balance = round($prom_note - $sum_deduction,3);
		   		$ending_balance = round($beginning_balance - $total_deduction,3);
		     	$sum_deduction += round($total_deduction,3);
		     	$m_row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_members_type WHERE mem_type_id = '$b_data[mem_type_id]'"));
	     		$m = date("m",strtotime($loan_date));
	     		$y = date("Y",strtotime($loan_date));

	     		if($m_row["mem_coll_date"] != "0" && $m_row["mem_coll_date2"] != "0"){
	     			$monthly_int = $interest_month / 2;
	     			$prin = $principal / 2;
	     			$end_bal = $ending_balance / 2;
	     		}else{
	     			$monthly_int = $interest_month;
	     			$prin = $principal;
	     			$end_bal = $ending_balance;
	     		}

	     		if($m_row["mem_coll_date"] != "0"){
		     		$date_today = date("Y-m-d", strtotime($y."-".$m."-".$m_row["mem_coll_date"]));
			     	$effectiveDate = date('Y-m-d H:i:s', strtotime("+".$counter." months", strtotime($date_today)));

			     	$insert_subsidiary = mysqli_query($conn, "INSERT INTO `tbl_subsidiary` (`loan_id`, `pensioneer_id`, `loan_num`, `month`, `beginning_balance`, `interest`, `principal`, `ending_balance`, `date_added`, `status`) VALUES ('$loan_id','$pensioneer', '$loan_num', '$counter', '$beginning_balance', '$monthly_int', '$prin', '$end_bal', '$effectiveDate','P')");
		     	}

		     	if($m_row["mem_coll_date2"] != "0"){
			     	$date_today2 = date("Y-m-d", strtotime($y."-".$m."-".$m_row["mem_coll_date2"]));
			     	$effectiveDate2 = date('Y-m-d H:i:s', strtotime("+".$counter." months", strtotime($date_today2)));

			     	$insert_subsidiary2 = mysqli_query($conn, "INSERT INTO `tbl_subsidiary` (`loan_id`, `pensioneer_id`, `loan_num`, `month`, `beginning_balance`, `interest`, `principal`, `ending_balance`, `date_added`, `status`) VALUES ('$loan_id','$pensioneer', '$loan_num', '$counter', '$beginning_balance', '$monthly_int', '$prin', '$end_bal', '$effectiveDate2','P')");
		     	}
		    
		}

		if($insert_subsidiary && $insert_subsidiary2){
			echo 1;
		}else{
			echo 0;
		}
	 
	}

	
}else{
	echo 0;
}


?>