<?php 
  include "../core/conn.php";
  $checkedVals = $_POST['checkedVals'];
  $or_num = $_POST['or_num'];
  $max_sub = $_POST['max_sub'];

  $payment_type = $_POST['payment_type'];
  $interest = $_POST['interest'];
  $enable_partial = "1";
  foreach ($checkedVals as $val) {
       if($max_sub==$val){
            $enable_partial ="0";
       }
  }

  if($payment_type=="Cash"){
    $hide_bank_data = "style='display: none;'";
  }else{
    $hide_bank_data = "";
  }


?>

  <form id="issue_OR_modal" method="post" class="form-horizontal" action="">
      <div class="row">

          <div id="order_items_layout" class="form-group col-sm-12" >
           
            <div class="card-header">
              <h6 class="modal-title"><i class="fas fa-cart-plus"></i> Add Official Receipt - OR no. <?php echo $or_num;?></h6>
            </div>
           <div class="card-body text-dark">
            <div class="col-sm-12">
              <div class="row">

               <div class="col-sm-6 mb-2">
                <div class="input-group input-group-sm">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm" style="background-color: #ffff">Date Issued:</span>
                  </div>
                  <input id="issue_date" type="date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" value="<?php  echo get_current_date();?>">
                </div>
              </div>
               <div class="col-sm-6 mb-2">
               </div>

               <div class="col-sm-6 mb-2" <?php echo  $hide_bank_data;?>>
                <div class="input-group input-group-sm">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Bank</label>
                  </div>
                  <select class="custom-select" id="select_bank">
                    <option value="0" selected>Select Bank</option>
                    <?php
                     $get_bank =  mysqli_query($conn, "SELECT * FROM `tbl_bank`");
                      while($row = mysqli_fetch_array($get_bank)){

                        ?>
                            <option value="<?php echo $row['bank_id']?>"><?php echo $row['bank_name']?></option>
                        <?php

                      }
                    ?>
                
                  </select>
                </div>
               </div>

                <div class="col-sm-6 mb-2"  <?php echo  $hide_bank_data;?>>
                <div class="input-group input-group-sm">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm" style="background-color: #ffff">Check No.</span>
                  </div>
                  <input id="check_num" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                </div>

              </div>

                       <div class="col-sm-12">
                         <div class="table-responsive-md">
                          <table id="tbl_items" class="table table-bordered table-striped" style="font-size: 15px;">
                          <thead class="thead-light">
                          <tr  style="word-break: break-all;">
                            <th>Reference No.</th>
                            <th>Due date</th>
                            <th>Penalty</th>
                            <th>Partial</th>
                            <th>Amount due</th>

                          </tr>

                          </thead>
                          
                          <tbody>
                            <?php
                            $total_due = 0;
                            $maximum_add_partial = 0;
                              foreach ($checkedVals as $val) {
                                 $get_subsidiary = mysqli_fetch_array(mysqli_query($conn, "SELECT a.date_added,sum(b.penalty_amount) as penalty_amount, a.principal,a.interest, sum(c.amount) as partial from `tbl_subsidiary` a left join tbl_payment_penalty b on a.subsidiary_id=b.subsidiary_id left join tbl_partial_payment c on a.subsidiary_id = c.subsidiary_id where a.subsidiary_id='$val'"));
                                $due_date = $get_subsidiary['date_added'];
                                $penalty_amount = $get_subsidiary['penalty_amount'];
                                $partial = $get_subsidiary['partial'];
                                $amt_due = $get_subsidiary['principal']+$get_subsidiary['interest']+$get_subsidiary['penalty_amount']-$partial;
                                $total_due +=$get_subsidiary['principal']+$get_subsidiary['interest']+$get_subsidiary['penalty_amount']-$partial;
                                $maximum_add_partial=$get_subsidiary['principal']+$get_subsidiary['interest'];
                                ?>
                                <tr>
                                  <td><?php echo $val;?></td>
                                  <td><?php echo $due_date;?></td>
                                  <td><?php echo $penalty_amount;?></td>
                                  <td><?php echo $partial;?></td>
                                  <td><?php echo $amt_due;?></td>
                                </tr>

                                <?php
                              }
                              $maximum_add_partial = $maximum_add_partial+$total_due;
                            ?>
                          </tbody>
                          <tfoot>
                            <tr>
                                <th colspan="4" style="text-align:right">Total Amount Due:</th>
                                <th><?php echo  $total_due;?></th>
                            </tr>
                        </tfoot>
                        </table>
                      </div>

                       </div>

               

                <div class="form-group col-sm-6">
                
                </div>
                 <div class="form-group col-sm-6">
                
                    <div class="input-group input-group-sm">
                     <div class="input-group-prepend">
                       <span class="input-group-text" id="basic-addon1">Payment </span>
                     </div>
                      <input id="payment" type="number" class="form-control"  aria-label="username" aria-describedby="basic-addon1" onkeyup="check_payment()">
                 
                  </div>
                </div>
                   <div class="form-group col-sm-6">
                
                </div>

                 <div class="form-group col-sm-6">
                
                    <div class="input-group input-group-sm">
                     <div class="input-group-prepend">
                       <span class="input-group-text" id="basic-addon1">Change </span>
                     </div>
                      <input id="change" type="number" class="form-control"  aria-label="username" aria-describedby="basic-addon1" readonly="" value="0">
                 
                  </div>
                </div>
                   <div class="form-group col-sm-6">
                
                </div>

                 <div class="form-group col-sm-6">
                
                 

                  <div class="input-group mb-3" id="add_partial" style="display:none">
        <div class="input-group-prepend">
          <div class="input-group-text">
            <input type="checkbox" aria-label="Checkbox for following text input" id="check_box_partial">
          </div>
        </div>
        <input type="text" class="form-control" aria-label="Text input with checkbox" value="Add as partial payment" readonly="">
      </div>
                </div>

                <div class="col-sm-12 modal-footer">
                  <div class="btn-group float-sm-right">
                   <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal_issue_OR"
                   onclick="confirm_or()" >Confirm</button>
                 </div>
                </div>
              </div>
              </div>
  
                 </div>
               </div>
             </div>
            <!-- /items -->

          </div>
          <!-- /.card-footer -->
  </form>

<!-- /.card-footer -->

<script type="text/javascript">

function apm_compute_amount(){
  var Percent = $("#penalty_type").val();
  var remaining_balance = "<?php echo /*$remaining_balance*/0;?>";
  var amount = (Percent/100) * remaining_balance;
  var real_amount = Math.round(amount * 100) / 100
  $("#percent").val(Percent);
  $("#amount").val(real_amount);

}

function check_payment(){

var payment = $("#payment").val();
var total_due = <?php echo $total_due;?>;
var enable_partial = <?php echo $enable_partial;?>;


if(payment==""){
$("#payment").css('color', 'black');
$("#change").val("0");
$("#add_partial").hide();
}else if(payment==total_due){
$("#payment").css('color', 'black');
$("#change").val("0");
$("#add_partial").hide();
}else if(payment>total_due){

if(enable_partial==0){
var change = payment-total_due;
var real_amount = Math.round(change * 100) / 100
$("#change").val(real_amount);
$("#payment").css('color', 'black');
}else{

$("#add_partial").show();
var change = payment-total_due;
var real_amount = Math.round(change * 100) / 100
$("#change").val(real_amount);
$("#payment").css('color', 'black');
}

}else{
$("#add_partial").hide();
$("#change").val("0");
$("#payment").css('color', 'red');
}

}

function confirm_or(){
  var payment_type = "<?php echo $payment_type;?>";
   var interest =Number("<?php echo $interest;?>");
  var payment = $("#payment").val();
  var change = $("#change").val();
  var issue_date = $("#issue_date").val();
  var is_partial = $("#check_box_partial").prop("checked");
  var bank_id = $("#select_bank").val();
  var check_num = $("#check_num").val();
  var total_due = <?php echo $total_due;?>;
  var max_add_partial =  Number("<?php echo $maximum_add_partial;?>");
  
if(payment_type=="Cash"){
  if(payment<total_due){
    if(payment>=interest){
   
          alert("insufficient payment.  Transaction will be considered as partial payment.");
         issue_final_Receipt_partial(payment,total_due,change,issue_date,"0",payment_type,bank_id,check_num);
    }else{
     
        alert("Cannot add partial payment that is below the interest rate.");
    }

  }else if(payment>total_due){
    if(is_partial){
     
        if(payment<=max_add_partial&change>=interest){
       issue_final_Receipt(total_due,change,issue_date,"1",payment_type,bank_id,check_num);
      }else{
            alert("System Error. Cannot add as partial payment Change exceeded Total amount or below the interest.");
      } 
    }else{
     issue_final_Receipt(total_due,change,issue_date,"0",payment_type,bank_id,check_num);
    }
  }else{
    if(is_partial){
  
     if(payment<=max_add_partial&change>=interest){
        issue_final_Receipt(payment,change,issue_date,"1",payment_type,bank_id,check_num);
      }else{
                alert("System Error. Cannot add as partial payment Change exceeded Total amount or below the interest.");
      } 

    }else{
    issue_final_Receipt(payment,change,issue_date,"0",payment_type,bank_id,check_num);
    }
  }

}else{
  if(payment<total_due){
    if(payment>=interest){
          alert("insufficient payment.  Transaction will be considered as partial payment.");
         issue_final_Receipt_partial(payment,total_due,change,issue_date,"0",payment_type,bank_id,check_num);
    }else{
         alert("System Error. Cannot add as partial payment Change exceeded Total amount or below the interest.");
    }

  }else if(check_num==""||bank_id==""){
    alert("Please fill-up required data");
  }
  else if(payment>total_due){
    if(is_partial){
    
        if(payment<=max_add_partial&change>=interest){
        issue_final_Receipt(total_due,change,issue_date,"1",payment_type,bank_id,check_num);
      }else{
         alert("System Error. Cannot add as partial payment Change exceeded Total amount or below the interest.");
      } 
    }else{
     issue_final_Receipt(total_due,change,issue_date,"0",payment_type,bank_id,check_num);
    }
  }else{
    if(is_partial){
      if(payment<=max_add_partial&change>=interest){

         issue_final_Receipt(payment,change,issue_date,"1",payment_type,bank_id,check_num);
      }else{
        alert("System Error. Cannot add as partial payment Change exceeded Total amount or below the interest.");
      } 
   
    }else{
    issue_final_Receipt(payment,change,issue_date,"0",payment_type,bank_id,check_num);
    }
  }

}


 
}





</script>