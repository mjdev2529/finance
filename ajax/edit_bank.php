<?php 

  include "../core/conn.php";

  $bank_id = $_POST["id"];

  $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_bank WHERE bank_id = '$bank_id'"));

?>


<div class="card-body">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-6 control-label">Bank Name:</label>

    <div class="col-12">
      <input type="text" class="form-control" id="edit_b_name" name="b_name" placeholder="Bank Name" value="<?php echo $data['bank_name']?>">
      <input type="hidden" class="form-control" id="edit_b_id" name="b_id" value="<?php echo $data['bank_id']?>">
    </div>
  </div>
</div>
<!-- /.card-body -->
<div class="card-footer">
  <button type="submit" id="btn-edit-bank" class="btn btn-secondary btn-flat btn-block">Save Changes</button>
</div>
<!-- /.card-footer -->