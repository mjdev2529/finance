<?php 

  include "../core/conn.php";

  $loan_id = $_POST["loan_id"];
  $subsidiary_id = $_POST["subsidiary_id"];
  $remaining_balance = $_POST["remaining_balance"];
  // $trans_id = $_POST["transaction_id"];
  // $get_remaining = mysql_fetch_array(mysql_query("SELECT sum(interest+principal) remaining_balance FROM `tbl_subsidiary` where loan_id='19' and status = 'P'"));
  $get_penalty_type = mysql_query("SELECT * from tbl_penalty_type");
?>

  <form id="add_transaction_modal" method="post" class="form-horizontal" action="">
                <div class="card-body bg-light">


                    <div class="row">

                <div id="order_items_layout" class="form-group col-sm-12">
                 <div class="card border-success" style="margin-top: 10px">
                   <div class="card-header bg-secondary">
                    <h6 class="modal-title"><i class="fas fa-cart-plus"></i>Add Penalty</h6>
                  </div>
                 <div class="card-body text-dark">
                  <div class="col-sm-12">
                    <div class="row">

                        <div class="form-group col-sm-6">
                   
                        <div class="input-group input-group-sm">
                         <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon1">Penalty </span>
                         </div>
                         <select class="custom-select" id="penalty_type" onchange="apm_compute_amount()">
                            <option selected value="0">Please Select Penalty</option>
                            <?php
                            while($row = mysql_fetch_array($get_penalty_type)){
                              ?>
                               <option value="<?php echo $row['penalty_type_id']?>"><?php echo $row['penalty_name']?></option>
                            <?php
                            }
                            ?>
                         <!--    <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option> -->
                          </select>
                        </div>
                       
                      </div>

                        <div class="form-group col-sm-6">
                   
                        <div class="input-group input-group-sm">
                         <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon1">Remaining Principal </span>
                         </div>
                         <input id="product_name" type="text" class="form-control" placeholder="item name" aria-label="username" aria-describedby="basic-addon1"  value="<?php echo  $remaining_balance;?>" readOnly>
                        </div>
                       
                      </div>

                      <div class="form-group col-sm-6">
                      
                          <div class="input-group input-group-sm">
                           <div class="input-group-prepend">
                             <span class="input-group-text" id="basic-addon1">Percent </span>
                           </div>
                            <input id="percent" type="number" class="form-control" placeholder="%" aria-label="username" aria-describedby="basic-addon1">
                       
                        </div>
                      </div>
                      <div class="form-group col-sm-6">
                      
                          <div class="input-group input-group-sm">
                           <div class="input-group-prepend">
                             <span class="input-group-text" id="basic-addon1">Amount </span>
                           </div>
                             <input id="amount" type="number" class="form-control" placeholder="Amount" aria-label="username" aria-describedby="basic-addon1" readonly>
                       
                        </div>
                      </div>
                        
                             <div class="form-group col-sm-12">
                      
                          <div class="input-group input-group-sm">
                      
                              <button class="btn btn-flat btn-primary btn-sm" onclick="add_penalty_modal()" type="button" data-dismiss="modal"></i> Add Penalty</button>
                              <button id="btn_delete_penalty" class="btn btn-flat btn-danger btn-sm" onclick="delete_penalty_modal()()" type="button"><i class="fa fa-trash-alt"></i> Delete</button>
                            </div>
                        </div>

                             <div class="col-sm-12">
                               <div class="table-responsive-md">
                                <table id="tbl_penalty" class="table table-bordered table-striped" style="font-size: 15px;">
                                <thead class="thead-light">
                                <tr  style="word-break: break-all;">
                                  <th ></th>
                                  <th >#</th>
                                  <th >Date added</th>                                 
                                  <th >Penalty</th>
                                  <th>%</th>
                                  <th>Penalty amount</th>
                                </tr>
                                </thead>
                                
                                <tbody>
                                </tbody>
                              
                              </table>
                            </div>

                             </div>
                    </div>
                    </div>
        
                       </div>
                     </div>
                   </div>
                  <!-- /items -->

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                
                </div>
                <!-- /.card-footer -->
              </form>

<!-- /.card-footer -->

<script type="text/javascript">

function apm_compute_amount(){
  var Percent = $("#penalty_type").val();
  var remaining_balance = "<?php echo $remaining_balance;?>";
  var amount = (Percent/100) * remaining_balance;
  var real_amount = Math.round(amount * 100) / 100
  $("#percent").val(Percent);
  $("#amount").val(real_amount);
}

$(document).ready(function()
  {
  	get_penalty();
  });

  function get_penalty(){
  	var subsidiary_id = <?php echo $subsidiary_id;?>;
   $("#tbl_penalty").DataTable().destroy();
    $("#tbl_penalty").dataTable({
      "paging": false,
      "dom": 't',
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/penalty.php",
        "data":{"subsidiary_id": subsidiary_id},
        "processing":true
      },
      "columns":[
       {
        "mRender": function(data,type,row){
          return "<input type='checkbox' name='add_modal_penalty' value='"+row.penalty_id+"'>";
        }
      },
       {
        "data":"count"
      },
      {
        "data":"date_added"
      },
      {
        "data":"penalty_name"
      },
      {
        "data":"percent"
      },
      {
        "data":"penalty_amount"
      }
      ]
    });
  }


function add_penalty_modal(){

//  $("#btn_add_penalty").prop("disabled", true);
  		var subsidiary_id = <?php echo $subsidiary_id;?>;
  		var penalty_type_id = $("#penalty_type").val();
  		var amount =  $("#amount").val();

       $.ajax({
          url: "../ajax/add_penalty.php",
          type: "post",
          data: {subsidiary_id:subsidiary_id,penalty_type_id:penalty_type_id,amount:amount},
          success: function (response) {
           //   $("#btn_add_penalty").prop("disabled", false);
            if(response==1){
              get_penalty();
              get_data_official_receipt();
               iziAlert("fa fa-check","Success! ,","A new item was added.","bottomLeft","success");
        
            }else{
              iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
          //    $("#btn_add_penalty").prop("disabled", false);
             console.log(textStatus, errorThrown);
          }
   });
 }

 function delete_penalty_modal(){
 	$("#btn_delete_penalty").prop("disabled", true);
    var url = "../ajax/delete_penalty.php";
    var x  = confirm("Are you sure to delete?");
    var checkedVals = [];
    $('input[name=add_modal_penalty]:checked').map(function() {
      checkedVals.push($(this).val());
    });

    if(x){
      if(checkedVals){
  
        $.post(url,{id: checkedVals},function(data){
            $("#btn_delete_penalty").prop("disabled", false);
          if(data == 1){
          	get_penalty();
          	get_data_official_receipt();
            iziAlert("fa fa-check","Success! ,","Selected item deleted.","bottomLeft","success");
         
          }else{
           iziAlert("fa fa-ban","Error! ,","Something was wrong.","bottomLeft","error");
          }
        });
      }else{
             $("#btn_delete_penalty").prop("disabled", false);
        iziAlert("fa fa-info","Warning! ,","No item selected.","bottomLeft","warning");
      }
    }
   
    }


</script>