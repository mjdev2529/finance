<?php 

  include "../core/conn.php";

  $fid = $_POST["id"];

  $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_fees WHERE fees_id = '$fid'"));

?>


<div class="card-body">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-6 control-label">Deduction Name:</label>
    <div class="col-12">
      <input type="text" class="form-control" id="edit_d_name" name="d_name" placeholder="Deduction Name" value="<?php echo $data['fees_name']?>">
      <input type="hidden" class="form-control" id="edit_b_id" name="f_id" value="<?php echo $data['fees_id']?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-6 control-label">Deduction Value:</label>
    <div class="col-12">
      <input type="number" class="form-control" id="edit_d_value" name="d_value" placeholder="Deduction Value" value="<?php echo $data['value']?>">
    </div>
  </div>

  <div class="form-group">
    <div class="col-12">

      <label for="inputEmail3" class="control-label mr-3">Preset:</label>
      <input type="checkbox" id="d_preset" value="1" <?php if($data['preset'] == 1){ echo "checked"; }?>>
    </div>
  </div>

</div>
<!-- /.card-body -->
<div class="card-footer">
  <button type="submit" id="btn-edit-deduct" class="btn btn-secondary btn-flat btn-block">Save Changes</button>
</div>
<!-- /.card-footer -->