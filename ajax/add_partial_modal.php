<?php 

  include "../core/conn.php";

  $loan_id = $_POST["loan_id"];
  $subsidiary_id = $_POST["subsidiary_id"];
  $remaining_balance = $_POST["remaining_balance"];
  // $trans_id = $_POST["transaction_id"];
  // $get_remaining = mysql_fetch_array(mysql_query("SELECT sum(interest+principal) remaining_balance FROM `tbl_subsidiary` where loan_id='19' and status = 'P'"));
  $get_penalty_type = mysqli_query($conn, "SELECT * from tbl_partial_type");
?>

  <form id="add_partial_modal" method="post" class="form-horizontal" action="">
                <div class="card-body bg-light">


                    <div class="row">

                <div id="order_items_layout" class="form-group col-sm-12">
                 <div class="card border-success" style="margin-top: 10px">
                   <div class="card-header bg-secondary">
                    <h6 class="modal-title"><i class="fas fa-cart-plus"></i>Add Partial</h6>
                  </div>
                 <div class="card-body text-dark">
                  <div class="col-sm-12">
                    <div class="row">

                      <div class="form-group col-sm-6">
                      
                          <div class="input-group input-group-sm">
                           <div class="input-group-prepend">
                             <span class="input-group-text" id="basic-addon1">Partial Amount </span>
                           </div>
                             <input id="amount_partial" type="number" class="form-control" placeholder="Amount" aria-label="username" aria-describedby="basic-addon1" onkeyup="check_amount()">
                       
                        </div>
                      </div>

                        <div class="form-group col-sm-6">
                      
                          <div class="input-group input-group-sm">
                           <div class="input-group-prepend">
                             <span class="input-group-text" id="basic-addon1">Remaining balance </span>
                           </div>
                             <input id="remaining_balance" type="number" class="form-control" placeholder="Amount" aria-label="username" value="<?php echo  $remaining_balance;?>" aria-describedby="basic-addon1" readonly="">
                       
                        </div>
                      </div>

                             <div class="col-sm-12">
                               <div class="table-responsive-md">
                                <table id="tbl_partial" class="table table-bordered table-striped" style="font-size: 15px;">
                                <thead class="thead-light">
                                <tr  style="word-break: break-all;">
                                  <th ></th>
                                  <th >#</th>
                                  <th >Date added</th>                                 
                                  <th >Amount</th>
                                </tr>
                                </thead>
                                
                                <tbody>
                                </tbody>
                              
                              </table>
                            </div>

                             </div>
                    </div>
                    </div>
        
                       </div>
                     </div>
                   </div>
                  <!-- /items -->

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                
                </div>
                <!-- /.card-footer -->
              </form>

<!-- /.card-footer -->

<script type="text/javascript">
$(document).ready(function()
  {
    get_partial();
  });

function check_amount(){
var amount = $("#amount_partial").val();
var remaining_balance = <?php echo $remaining_balance;?>;

  if(amount>remaining_balance){
      $("#amount_partial").css('color', 'red');
  }else{
      $("#amount_partial").css('color', 'black');
  }
}


  function get_partial(){
  	var subsidiary_id = <?php echo $subsidiary_id;?>;
   $("#tbl_partial").DataTable().destroy();
    $("#tbl_partial").dataTable({
      "paging": false,
      "dom": 't',
      "ajax":{
        "type":"POST",
        "url":"../ajax/datatables/partial.php",
        "data":{"subsidiary_id": subsidiary_id},
        "processing":true
      },
      "columns":[
       {
        "mRender": function(data,type,row){
          return "<input type='checkbox' name='add_modal_partial' value='"+row.partial_payment_id+"'>";
        }
      },
       {
        "data":"count"
      },
      {
        "data":"date_added"
      },
      {
        "data":"amount"
      }
      ]
    });
  }


function add_partial_modal(){

      var subsidiary_id = <?php echo $subsidiary_id;?>;
      var remaining_balance = <?php echo $remaining_balance;?>;
  		var amount =  $("#amount_partial").val();

      if(amount<0){
        alert("Please fill-in amount");
      }else if(amount>remaining_balance){
        alert("Invalid data entry! Amount is greater than remaining balance.");
      }else{
            $.ajax({
          url: "../ajax/add_partial.php",
          type: "post",
          data: {subsidiary_id:subsidiary_id,amount:amount},
          success: function (response) {
           //   $("#btn_add_partial").prop("disabled", false);
            if(response==1){
           //   get_partial();
              get_data_official_receipt();
               iziAlert("fa fa-check","Success! ,","A new item was added.","bottomLeft","success");
        
            }else{
              iziAlert("fa fa-times","Error! ,","Something was wrong.","bottomLeft","error");
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
           //   $("#btn_add_partial").prop("disabled", false);
             console.log(textStatus, errorThrown);
          }
        });

      }
   
 }

 function delete_partial_modal(){
 	$("#btn_delete_penalty").prop("disabled", true);
    var url = "../ajax/delete_partial.php";
    var x  = confirm("Are you sure to delete?");
    var checkedVals = [];
    $('input[name=add_modal_partial]:checked').map(function() {
      checkedVals.push($(this).val());
    });

    if(x){
      if(checkedVals){
  
        $.post(url,{id: checkedVals},function(data){
            $("#btn_delete_penalty").prop("disabled", false);
          if(data == 1){
          	get_partial();
          	get_data_official_receipt();
            iziAlert("fa fa-check","Success! ,","Selected item deleted.","bottomLeft","success");
         
          }else{
           iziAlert("fa fa-ban","Error! ,","Something was wrong.","bottomLeft","error");
          }
        });
      }else{
             $("#btn_delete_penalty").prop("disabled", false);
        iziAlert("fa fa-info","Warning! ,","No item selected.","bottomLeft","warning");
      }
    }
   
    }


</script>