<?php

include "../../core/conn.php";
 $start_date = $_POST['start_date'];
 $end_date = $_POST['end_date'];

$count = 1;
$response["data"] = array();
	$get_loans = mysqli_query($conn, "SELECT 
		a.loan_id
		,a.loan_no
		, count(b.subsidiary_id) total_term
		,sum(case when b.status = 'F' then 1 else 0 end) as total_paid
		,a.date
		,a.loan_no
		,a.code_no
		,a.pensioneers_id
		,a.status
		,a.net_proceeds
from tbl_loan a left join tbl_subsidiary b on a.loan_id=b.loan_id 
where (date(a.date)>='$start_date' and date(a.date) <='$end_date')
group by a.loan_id order by a.loan_no ASC");

	while ($row = mysqli_fetch_array($get_loans)) {
			$ongoing = "(".$row["total_paid"]."/".$row["total_term"].")";
		if($row["status"] == "" || $row["status"] == 0){
			$status = "<span class='text-primary'>$ongoing Ongoing </span>";
		}else if($row["status"] == 2){
			$status = "<span class='text-danger'>Canceled</span>";
		}else{
			$status = "<span class='text-success'>Finished</span>";
		}
		
		if($row["total_term"]==0){
			$status = "<span class='text-secondary'>N.A.</span>";
		}else{
			if($row["total_paid"]==$row["total_term"]){
			$status = "<span class='text-success'>$ongoing Finished</span>";
			}
		}


		$list["count"] = $count++.".";
		// $list['checkbox'] = $checkbox;
		$list["id"] = $row["loan_id"];
		$list["date"] = date('Y-m-d',strtotime($row["date"]));
		$list["loan_no"] = $row["loan_no"];
		$list["code_no"] = $row["code_no"];
		$list["pensioneers_name"] = get_pensioneer_name($row["pensioneers_id"], $conn);
		$list["status"] = $status;
		$list["stat"] = $row["status"];
		$list["net_proceeds"] = $row["net_proceeds"];

		array_push($response["data"], $list);
	}
echo json_encode($response);
?>