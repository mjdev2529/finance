<?php

	include "../../core/conn.php";
	
	$banks = mysqli_query($conn, "SELECT * FROM tbl_bank");
	$count = 1;
	$test = $_POST["test"];
	$response["data"] = array();
	while ($data = mysqli_fetch_array($banks)) {

		$list[] = array();
		$list["count"] = $count++.".";
		$list["bank_id"] = $data["bank_id"];
		$list["bank_name"] = $data["bank_name"];

		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>