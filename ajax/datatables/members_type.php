<?php

	include "../../core/conn.php";
	
	$mem_type = mysqli_query($conn, "SELECT * FROM tbl_members_type");
	$count = 1;
	$response["data"] = array();
	while ($data = mysqli_fetch_array($mem_type)) {

		$list[] = array();
		$list["count"] = $count++.".";
		$list["type_id"] = $data["mem_type_id"];
		$list["name"] = ucfirst($data["mem_type_name"]);

		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>