<?php

	include "../../core/conn.php";
	
	$banks = mysqli_query($conn, "SELECT * FROM tbl_fees");
	$count = 1;
	$test = $_POST["test"];
	$response["data"] = array();
	while ($data = mysqli_fetch_array($banks)) {

		if($data["preset"] == 1){
			$dis = "disabled";
		}else{
			$dis = "";
		}

		$list[] = array();
		$list["count"] = $count++.".";
		$list["fees_id"] = $data["fees_id"];
		$list["fees_name"] = $data["fees_name"];
		$list["fees_value"] = number_format($data["value"],2);
		$list["disable"] = $dis;

		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>