<?php

	include "../../core/conn.php";
	
	$users = mysqli_query($conn, "SELECT * FROM tbl_users");
	$count = 1;
	$response["data"] = array();
	while ($data = mysqli_fetch_array($users)) {

		if($data["status"] == 0){
			$stat = "<span class='text-success'><i class='fa fa-check-circle'></i> Active</span>";
		}else{
			$stat = "<span class='text-danger'><i class='fa fa-ban'></i> Disabled</span>";
		}

		if($data["role"] == 0){
			$role = "Admin";
		}else if($data["role"] == 1){
			$role = "Owner";
		}else{
			$role = "Secretary";
		}

		$list[] = array();
		$list["count"] = $count++.".";
		$list["u_id"] = $data["user_id"];
		$list["name"] = ucfirst($data["lname"]).", ".ucfirst($data["fname"]);
		$list["role"] = $role;
		$list["urole"] = $data["role"];
		$list["status"] = $stat;

		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>