<?php

include "../../core/conn.php";
$pen_id = $_POST['pen_id'];
$loan_id = $_POST['loan_id'];
// $official_receipt = $_POST['official_receipt'];

if($loan_id == 0 /*|| $official_receipt == 1*/){
	$loan = " and status != 'P'";
}else{
	$loan = " and loan_id = '$loan_id'";
}

//$get_sub = mysqli_query("SELECT * FROM tbl_subsidiary where pensioneer_id = '$pen_id' $loan order by subsidiary_id, date_added ASC");
$get_sub = mysqli_query($conn, "SELECT a.subsidiary_id,a.date_added,a.beginning_balance,a.interest,a.principal,a.status,sum(b.penalty_amount) as penalty_amount
from `tbl_subsidiary` a left join tbl_payment_penalty b on a.subsidiary_id=b.subsidiary_id
where a.loan_id = '$loan_id' and a.pensioneer_id='$pen_id' group by a.subsidiary_id");

$count = 1;
$response["data"] = array();


	while ($row = mysqli_fetch_array($get_sub)) {


		$get_partial = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(amount) as partial_amount from tbl_partial_payment where subsidiary_id='$row[subsidiary_id]'"));


		$now = date("Y-m-d");
		$due_date = date('Y-m-d',strtotime($row["date_added"]));
		$partial_amount = $get_partial["partial_amount"];
		$interest = $row["interest"];
		$penalty =  $row["penalty_amount"];
		$principal =$row["principal"];

		if($due_date < $now){
			$overdue = 1;
		}else{
			$overdue = 0;
		}

		if($row["status"]=="F"){
			$disable = "style='display:none;'";
		}else{
			$disable = "";
		}

//remaining balance for penalty modal
		if($partial_amount>0){
			$partial = 1;
				if($partial_amount>$interest){
					$list["remaining_balance"] =$principal+$interest-$partial_amount;
				}else{
					$list["remaining_balance"] =$principal;	
				}
	
		}else{
			$partial = 0;
			$list["remaining_balance"] =$principal;	
		}

//remaining balance for partial modal
		if($partial_amount>0){
			$partial = 1;
				if($partial_amount>$interest){
					$list["remaining_balance_partial"] =$principal+$interest+$penalty-$partial_amount;
				}else{
					$list["remaining_balance_partial"] =$principal+$interest+$penalty-$partial_amount;	
				}
	
		}else{
			$partial = 0;
			$list["remaining_balance_partial"] =$principal+$interest+$penalty-$partial_amount;	
		}



		$list["count"] = $count++.".";
		$list["id"] = $row["subsidiary_id"];
		$list["penalty"] = $row["penalty_amount"];
		$list["subsidiary_id"] = $row["subsidiary_id"];
		$list["date_added"] = date('Y-m-d',strtotime($row["date_added"]));
		$list["BB"] = $row["beginning_balance"];
		$list["interest"] = number_format($interest,2);
		$list["principal"] = number_format($principal,2);
		$list["EB"] = number_format($interest + $principal+ $penalty,2);

		$list["disable"] = $disable;
		$list["status"] = $row["status"];
		$list["overdue"] = $overdue;
		$list["partial"] = $partial;

		array_push($response["data"], $list);
	}
echo json_encode($response);

?>