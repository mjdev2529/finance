<?php

	include "../../core/conn.php";
	
	$borrowers = mysqli_query($conn, "SELECT * FROM tbl_borrowers");
	$count = 1;
	$response["data"] = array();
	while ($data = mysqli_fetch_array($borrowers)) {
		$get_data = mysqli_fetch_array(mysqli_query($conn, "SELECT mem_type_name FROM tbl_members_type WHERE mem_type_id = '$data[mem_type_id]'"));
		$list[] = array();
		$list["count"] = $count++.".";
		$list["borrower_id"] = $data["borrower_id"];
		$list["b_name"] = ucfirst($data["b_lname"]).", ".ucfirst($data["b_fname"]);
		$list["b_type"] = "$get_data[0]";

		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>