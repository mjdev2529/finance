<?php

	include "../../core/conn.php";
	$loan_id = $_POST["loan_id"];
	
	$count = 1;
	$fees_sql = mysqli_query($conn, "SELECT * FROM tbl_loan_deductions WHERE loan_id = '$loan_id'");
	$response["data"] = array();
	$total = 0;
	while ($fee_data = mysqli_fetch_array($fees_sql)) {
		$fee_name = mysqli_fetch_array(mysqli_query($conn, "SELECT fees_name, preset FROM tbl_fees WHERE fees_id = '$fee_data[fee_id]'"));
		$total += $fee_data["deduction_amount"];
		$list[] = array();
		$list["count"] = $count++.".";
		$list["d_id"] = $fee_data["loan_deduction_id"];
		$list["d_name"] = $fee_name[0];
		$list["d_amount"] = number_format($fee_data["deduction_amount"],2);
		$list["preset"] = $fee_name[1];
		$list["total_deduct"] = $total;

		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>